package group_1_tower_defense;

import group_1_tower_defense.game_entities.Enemy;
import group_1_tower_defense.game_entities.OBJFactory;
import group_1_tower_defense.game_entities.Level;
import group_1_tower_defense.game_entities.Level.PopEnemies;
import group_1_tower_defense.game_entities.Map;
import group_1_tower_defense.game_entities.MapElement;
import group_1_tower_defense.game_entities.Projectile;
import group_1_tower_defense.game_entities.Tower;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Parser 
{	
	private File fXmlFile;
	DocumentBuilder dBuilder;
	DocumentBuilderFactory dbFactory;
	Document doc;

	public void loadFile(String toParse)
	{
		try
		{
			fXmlFile = new File(toParse);
			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		String lp = (((Element)(((Node)(doc.getElementsByTagName(XML_TDS)).item(0)))).getAttribute(XML_STARTLIFE));
		String mp = (((Element)(((Node)(doc.getElementsByTagName(XML_TDS)).item(0)))).getAttribute(XML_STARTMONEY));
		Game.setLifePoints(lp == null ? life : Integer.valueOf(lp));
		Game.setMoneyPoints(mp == null ? money : Integer.valueOf(mp));
		parseEnemies();
		parseProjectiles();
		parseTowers();
		extractMap();
		parseLevels();
		parseVisuals();
	}
	private void parseEnemies()
	{

		NodeList nList = doc.getElementsByTagName(XML_ENEMY);

		for (int temp = 0; temp < nList.getLength(); temp++) 
		{
			Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) 
			{
				Element eElement = (Element) nNode;
				Enemy.newTypeOfEnemy(Integer.valueOf(eElement.getAttribute(XML_VISUALID)), 
						Integer.valueOf(eElement.getAttribute(XML_LIFE)), 
						Integer.valueOf(eElement.getAttribute(XML_SPEED)), 
						Integer.valueOf(eElement.getAttribute(XML_DAMAGE)),
						(eElement.getAttribute(XML_FLIES)).equals("false") ? false : true,
								Integer.valueOf(eElement.getAttribute(XML_GIVMONEY)));

			}
		}
		System.out.print(nList.getLength() + " type of enemies " + "created.\n");
	}	
	private void parseProjectiles()
	{

		NodeList nList = doc.getElementsByTagName(XML_PROJECTILE);

		for (int temp = 0; temp < nList.getLength(); temp++) 
		{
			Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) 
			{
				Element eElement = (Element) nNode;
				Projectile.newTypeOfProjectile(Integer.valueOf(eElement.getAttribute(XML_VISUALID)), 
						Integer.valueOf(eElement.getAttribute(XML_DAMAGE)), 
						Integer.valueOf(eElement.getAttribute(XML_SPEED)), 
						Integer.valueOf(eElement.getAttribute(XML_AREA)), 
						Integer.valueOf(eElement.getAttribute(XML_SLOW)));

			}
		}
		System.out.print(nList.getLength() + " type of projectiles" + " created.\n");
	}	
	private void parseTowers()
	{

		NodeList nList = doc.getElementsByTagName(XML_TOWER);

		for (int temp = 0; temp < nList.getLength(); temp++) 
		{
			Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) 
			{
				Element eElement = (Element) nNode;
				Tower.newTypeOfTower(Integer.valueOf(eElement.getAttribute(XML_VISUALID)), 
						eElement.getAttribute(XML_NAME) == null ? name : eElement.getAttribute(XML_NAME),
						eElement.getAttribute(XML_LEVELUP) == null ? levelup : Integer.valueOf(eElement.getAttribute(XML_LEVELUP)),
						Integer.valueOf(eElement.getAttribute(XML_PROJECTILEID)), 
						Integer.valueOf(eElement.getAttribute(XML_PRICE)), 
						Integer.valueOf(eElement.getAttribute(XML_RELOAD)), 
						Integer.valueOf(eElement.getAttribute(XML_RANGE)),
								eElement.getAttribute(XML_NICON), eElement.getAttribute(XML_SICON));
			}
		}
		System.out.print(nList.getLength() + " type of towers" + " created.\n");
	}

	public Level []parseLevels()
	{

		NodeList nList = doc.getElementsByTagName(XML_LEVEL);
		List<Level> levels = new ArrayList<Level>();
		for (int temp = 0; temp < nList.getLength(); temp++) 
		{
			Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) 
			{
				Element eElement = (Element) nNode;
				Level newLevel = new Level();

				newLevel.timeBeforeStarting = (Integer.valueOf(eElement.getAttribute(XML_TIME)));
				newLevel.name = eElement.getAttribute(XML_NAME);

				NodeList nList2 = eElement.getElementsByTagName(XML_POPENEMIES);
				List<PopEnemies> popEnemies = new ArrayList<PopEnemies>();
				for (int temp2 = 0; temp2 < nList2.getLength(); temp2++) 
				{
					Node nNode2 = nList2.item(temp2);
					if (nNode2.getNodeType() == Node.ELEMENT_NODE)
					{
						Element eElement2 = (Element) nNode2;
						PopEnemies newPopEnemy = newLevel.new PopEnemies();
						newPopEnemy.enemiesID = (Integer.valueOf(eElement2.getAttribute(XML_ID)));
						newPopEnemy.enemiesNumber = (Integer.valueOf(eElement2.getAttribute(XML_NUMBER)));
						popEnemies.add(newPopEnemy);
						System.out.print("Pops " + newPopEnemy.enemiesNumber + " of id " + newPopEnemy.enemiesID + "\n");
					}
				}
				PopEnemies []popEnemiesArray = new PopEnemies[popEnemies.size()];
				for (int i = 0; i < popEnemies.size(); i++)
					popEnemiesArray[i] = popEnemies.get(i);
				newLevel.enemiesToPop = popEnemiesArray;
				System.out.print("Level : " + newLevel.name + " created!.\n");
				levels.add(newLevel);
			}
		}
		Level[] levelArray = new Level[levels.size()];
		for (int i = 0; i < levels.size(); i++)
			levelArray[i] = levels.get(i);
		Game.setLevels(levelArray);
		return (levelArray);
	}

	public void parseVisuals()
	{
		NodeList nList = doc.getElementsByTagName(XML_VISUAL);

		for (int temp = 0; temp < nList.getLength(); temp++) 
		{
			Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) 
			{
				Element eElement = (Element) nNode;
				int id = Integer.valueOf(eElement.getAttribute(XML_ID));
				String texture = eElement.getAttribute(XML_TEXTURE);
				String model = eElement.getAttribute(XML_MODEL);
				String type = eElement.getAttribute(XML_TYPE);
				OBJFactory.getSingleton().setOBJloader(type, id, model, texture);
			}
		}
		System.out.print(nList.getLength() + " Visuals" + " stored.\n");
	}

	public Map extractMap()
	{
		Element eElement = (Element) doc.getDocumentElement();
		NodeList nlList = eElement.getElementsByTagName(XML_MAP);
		Map res = null;

		if (nlList.getLength() > 0)
		{
			Node nValue = (Node) nlList.item(0);
			int mapWidth = (nValue.getAttributes().getNamedItem(XML_WIDTH) != null ? 
					Integer.valueOf(nValue.getAttributes().getNamedItem(XML_WIDTH).getTextContent()) : 0);
			int mapHeight = (nValue.getAttributes().getNamedItem(XML_HEIGHT) != null ?
					Integer.valueOf(nValue.getAttributes().getNamedItem(XML_HEIGHT).getTextContent()) : 0);
			int mapSquareSize = (nValue.getAttributes().getNamedItem(XML_SQUARESIZE) != null ?
					Integer.valueOf(nValue.getAttributes().getNamedItem(XML_SQUARESIZE).getTextContent()) : 0);

			String gridString = (getTagValue(XML_GRID, doc.getDocumentElement()));
			if (gridString == null)
				return null;
			gridString = gridString.replace("\t", "");
			gridString = gridString.replace(" ", "");
			gridString = gridString.replace("\n", "");

			MapElement [][]grid = new MapElement[mapHeight][mapWidth];

			for (int i = 0, j; i < mapHeight; i++)
				for (j = 0 ; j < mapWidth; j++)
					grid[i][j] = new MapElement(j, i, Integer.valueOf(gridString.substring(((i * mapWidth) + j),((i * mapWidth) + j) + 1)));
			System.out.print("\nFollowing map stored : \n");
			for (int i = 0, j; i < mapHeight; i++, System.out.print("\n"))
				for (j = 0 ; j < mapWidth; j++)
					System.out.print(grid[i][j].getType());
			res = new Map(mapWidth, mapHeight, grid);
			res.setSquareSize(mapSquareSize);
		}
		Game.setMap(res);
		return res;
	}

	private static String getTagValue(String sTag, Element eElement) 
	{
		if (eElement.getElementsByTagName(sTag).item(0) == null)
			return null;
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();

		Node nValue = (Node) nlList.item(0);

		return nValue.getNodeValue();
	}
	
	private static final int life = 100;
	private static final int levelup = -1;
	private static final int money = 10;
	private static final String name = "Standard Name";
	
	private static final String XML_PROJECTILE = "Projectile";
	private static final String XML_PROJECTILEID = "projectileId";
	private static final String XML_TDS = "TowerDefenseSettings";
	private static final String XML_ID = "id";
	private static final String XML_TYPE = "type";
	private static final String XML_TOWER = "Tower";
	private static final String XML_GRID = "grid";
	private static final String XML_LEVEL = "Level";
	private static final String XML_MAP = "Map";
	private static final String XML_WIDTH = "width";
	private static final String XML_HEIGHT ="height";
	private static final String XML_SQUARESIZE = "mapVisualSize";
	private static final String XML_VISUALID = "visual";
	private static final String XML_TEXTURE = "texture";
	private static final String XML_MODEL = "model";
	private static final String XML_TIME = "time";
	private static final String XML_NAME = "name";
	private static final String XML_NUMBER = "number";
	private static final String XML_POPENEMIES = "popEnemies";
	private static final String XML_RELOAD = "reload";
	private static final String XML_RANGE = "range";
	private static final String XML_PRICE = "price";
	private static final String XML_FLIES = "flies";
	private static final String XML_AREA = "area";
	private static final String XML_SLOW = "slow";
	private static final String XML_ENEMY = "Enemy";
	private static final String XML_STARTLIFE = "startingLifePoints";
	private static final String XML_STARTMONEY = "startingMoneyPoints";
	private static final String XML_LIFE = "life";
	private static final String XML_SPEED = "speed";
	private static final String XML_GIVMONEY = "givenMoney";
	private static final String XML_DAMAGE = "damage";
	private static final String XML_LEVELUP = "levelup";
	private static final String XML_VISUAL = "visual";
	private static final String XML_NICON = "normalIcon";
	private static final String XML_SICON = "selectedIcon";
}
