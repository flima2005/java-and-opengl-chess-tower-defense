package group_1_tower_defense.displayable_entities;
import group_1_tower_defense.Game;
import group_1_tower_defense.game_entities.Enemy;
import group_1_tower_defense.game_entities.MapElement;
import group_1_tower_defense.game_entities.MapEntityAbstract;
import group_1_tower_defense.game_entities.Projectile;
import group_1_tower_defense.game_entities.Tower;

import java.io.*;

import javax.imageio.ImageIO;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.glu.GLUquadric;

import java.awt.Graphics2D;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.awt.event.*;

/**
 * 
 * @author Frank Lima
 *
 */
public class Renderer implements GLEventListener,  KeyListener, MouseListener, MouseMotionListener
{
	/**
	* This class prepares and renders the 3D objects to the screen
	* as well as takes/controls and manages input and output from other classes in order to display
	*/

	private GLU glu = new GLU();
	float x = 1;
	float y = 1;
	float z = 1;

	float objectX = 0;
	float objectY = 0;

	private GLUquadric quadric;
	private float[] LightPos = {0.0f, 200.0f, 550.0f, 10.0f};   // Light Position 

	private float[] LightAmb = {0.6f, 0.6f, 0.6f, 1.0f};   // Ambient Light Values
	private float[] LightDif = {0.9f, 0.9f, 0.9f, 0.9f};   // Diffuse Light Values
	private float[] LightSpc = {0.2f, 0.2f, 0.2f, 0.2f}; // Specular Light Values

	private float[] MatAmb = {0.4f, 0.4f, 0.4f, 1.0f};    // Material - Ambient Values
	private float[] MatDif = {0.9f, 0.9f, 0.9f, 0.9f};    // Material - Diffuse Values
	private float[] MatSpc = {0.6f, 0.6f, 0.6f, 1.0f};    // Material - Specular Values
	private float[] MatShn = {1.0f};                      // Material - Shininess

	BufferedImage bufferedImage = null;
	int w;
	int h;
	File  file = new File("assets/bg.bmp");

	GLCanvas canvas;
	GLAutoDrawable glDraw;
	GL2 gl;
	ByteBuffer bb;

	ArrayList <OBJloader> loader = new ArrayList<OBJloader>();
	ArrayList<Float> selectedX = new ArrayList<Float>(); 
	ArrayList<Float> selectedY = new ArrayList<Float>();

	OBJloader playingField;
	int viewport[] = new int[4];
	double mvmatrix[] = new double[16];
	double projmatrix[] = new double[16];
	int realy = 0;// GL y coord pos
	double wcoord[] = new double[4];// wx, wy, wz;// returned xyz coords

	float getOriginX;
	float getOriginY;

	float lastX;
	float lastY;

	//floor ranges
	OBJloader range3 = new OBJloader("assets/range3.obj","assets/compass.jpg");
	OBJloader range5 = new OBJloader("assets/range5.obj","assets/compass.jpg");
	OBJloader range4 = new OBJloader("assets/range4.obj","assets/compass.jpg");
	OBJloader range2 = new OBJloader("assets/range2.obj","assets/compass.jpg");
	
	//explosion death
	OBJloader death = new OBJloader("assets/death.obj","assets/death.jpg");
	
	
	/**
	 * Constructor for the Renderer.  Must take in a canvas to enable its drawning and
	 * different GL listeners and input.
	 * @param canvas
	 */
	public Renderer(GLCanvas canvas) 
	{
		this.canvas = canvas;
	}

	/**
	 * When OpenGL is first created with GLEventListener, init is first called
	 * init is used to create all the initial values used for the rendering process
	 * The playing field and file/byte buffers are created
	 */
	public void init(GLAutoDrawable gLDrawable) 
	{
		System.out.println("INIT CALLED");
		gl = gLDrawable.getGL().getGL2();
		//loader.add(new OBJloader("assets/PlayingField.obj","assets/Crema_Timber.jpg"));
		playingField = new OBJloader("assets/PlayingField.obj","assets/Crema_Timber.jpg");

		try 
		{
			bufferedImage = ImageIO.read(file);
			w = bufferedImage.getWidth();
			h = bufferedImage.getHeight();
		} 

		catch (IOException e) 
		{
			e.printStackTrace();
		}

		WritableRaster raster = Raster.createInterleavedRaster (DataBuffer.TYPE_BYTE, w, h, 4, null);
		ComponentColorModel colorModel = new ComponentColorModel (ColorSpace.getInstance(ColorSpace.CS_sRGB), new int[] {8,8,8,8}, true, false, ComponentColorModel.TRANSLUCENT, DataBuffer.TYPE_BYTE);
		BufferedImage dukeImg = new BufferedImage (colorModel, raster, false, null);

		Graphics2D g = dukeImg.createGraphics();
		g.drawImage(bufferedImage, null, null);
		DataBufferByte dukeBuf = (DataBufferByte)raster.getDataBuffer();
		byte[] dukeRGBA = dukeBuf.getData();
		bb = ByteBuffer.wrap(dukeRGBA);
		bb.position(0);
		bb.mark();
		
		gl.glEnable(GL2.GL_LIGHTING);
		gl.glEnable(GL2.GL_LIGHT0);


	}

	/**
	 * This is the main OpenGL rendering function, it is automatically called every frame
	 * This function is hardware accelerated.
	 * In the display function, the background image is rendered, models are loaded into memory
	 * and light calculations are done.  The 3D rendering pulled from the Game Entities is
	 * calculated and displayed here as well.
	 */
	public void display(GLAutoDrawable gLDrawable) 
	{
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);


		/*BACKGROUND IMAGE*/
		
		gl.glLoadIdentity();
		gl.glOrtho(0, 300, 300, 0, 0, 1);
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glDisable(GL2.GL_DEPTH_TEST);
		gl.glClearColor(0.0f,0.0f,0.0f, 0.0f);  

		gl.glBindTexture(GL2.GL_TEXTURE_2D, 13);
		gl.glPixelStorei(GL2.GL_UNPACK_ALIGNMENT, 1);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_S, GL2.GL_CLAMP);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_WRAP_T, GL2.GL_CLAMP);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MAG_FILTER, GL2.GL_LINEAR);
		gl.glTexParameteri(GL2.GL_TEXTURE_2D, GL2.GL_TEXTURE_MIN_FILTER, GL2.GL_LINEAR);
		gl.glTexEnvf(GL2.GL_TEXTURE_ENV, GL2.GL_TEXTURE_ENV_MODE, GL2.GL_REPLACE);  //disable this and uses environment lighting
		gl.glTexImage2D (GL2.GL_TEXTURE_2D, 0, GL2.GL_RGBA, w, h, 0, GL2.GL_RGBA, GL2.GL_UNSIGNED_BYTE, bb);

		int left = 60;
		int top = 75;

		gl.glEnable(GL2.GL_TEXTURE_2D);

		gl.glBindTexture (GL2.GL_TEXTURE_2D, 13);
		gl.glBegin (GL2.GL_POLYGON);

		gl.glTexCoord2d (0, 0);
		gl.glVertex2d (left,top);
		gl.glTexCoord2d(1,0);
		gl.glVertex2d (left + 180, top);//w
		gl.glTexCoord2d(1,1);
		gl.glVertex2d (left + 180, top + 150);//w h
		gl.glTexCoord2d(0,1);
		gl.glVertex2d (left, top + 150);//h
		gl.glEnd ();	
		gl.glFlush();  


		
		/*MODELS LOADED HERE*/
		gl.glLoadIdentity();

		gl.glTranslatef(0.0f, -9.0f, -150.0f); 

		//Rotation Function for viewport
		gl.glRotatef(15f+y, 45f, 0f,0f);
		gl.glRotatef(0f+x, 0f, 1f,0f); //15 45 0 0 

		//algorithms used to compute mouse in 3D space
		gl.glGetIntegerv(GL2.GL_VIEWPORT, viewport, 0);
		gl.glGetDoublev(GL2.GL_MODELVIEW_MATRIX, mvmatrix, 0);
		gl.glGetDoublev(GL2.GL_PROJECTION_MATRIX, projmatrix, 0);



		/* LIGHTING CALCULATED HERE*/
		
		gl.glShadeModel(GL2.GL_SMOOTH);            // Enable Smooth Shading
		gl.glClearColor(0.0f, 0.0f, 0.0f, 0.5f);  // Black Background 0.5f
		gl.glClearDepth(1.0f);                    // Depth Buffer Setup
		gl.glClearStencil(0);                     // Stencil Buffer Setup
		gl.glEnable(GL2.GL_DEPTH_TEST);            // Enables Depth Testing
		gl.glDepthFunc(GL2.GL_LEQUAL);             // The Type Of Depth Testing To Do

		gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST);  

		gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_POSITION, LightPos, 0); // Set Light1 Position
		gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_AMBIENT, LightAmb, 0);  // Set Light1 Ambience
		gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_DIFFUSE, LightDif, 0);  // Set Light1 Diffuse
		gl.glLightfv(GL2.GL_LIGHT1, GL2.GL_SPECULAR, LightSpc, 0); // Set Light1 Specular
		gl.glEnable(GL2.GL_LIGHT1);                               // Enable Light1
		gl.glEnable(GL2.GL_LIGHTING);                             // Enable Lighting

		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_AMBIENT, MatAmb, 0);  // Set Material Ambience
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_DIFFUSE, MatDif, 0);  // Set Material Diffuse
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SPECULAR, MatSpc, 0); // Set Material Specular
		gl.glMaterialfv(GL2.GL_FRONT, GL2.GL_SHININESS, MatShn, 0);// Set Material Shininess

		gl.glCullFace(GL2.GL_BACK);                // Set Culling Face To Back Face
		gl.glEnable(GL2.GL_CULL_FACE);             // Enable Culling
		gl.glClearColor(0.9f, 0.9f, 0.9f, 0.9f);  // Set Black

		quadric = glu.gluNewQuadric();            // Initialize Quadratic
		glu.gluQuadricNormals(quadric, GL2.GL_SMOOTH); // Enable Smooth Normal Generation
		glu.gluQuadricTexture(quadric, false);        // Disable Auto Texture Coords



		
		/* 3D RENDERING COMPUTED HERE */
		//Render base playing field model before major rendering
		playingField.DrawModel(gl,0);

		
		/* This is our translation and rendering function for all of our objects */
		for (MapEntityAbstract anEntity : Game.getDisplayableItems())
		{	
			gl.glTranslatef(50f, 0f, 50f);

			//float X Y and Z for only rendering computation
			float renderX = -anEntity.getY()*9.0f;
			float renderY = 0;
			float renderZ = -anEntity.getX()*9.0f;


			//Check to see if the enemy is flying or not and adjust Y axis for flying piece
			if (anEntity instanceof Enemy && ((Enemy)anEntity).isFlying())
				renderY = 17.f;

			else if (anEntity instanceof Projectile && ((Projectile)anEntity).getTarget().isFlying())
				renderY = 16.f;


			//Change position for local X Y Z functions, render and re-adjust back to origin
			gl.glTranslatef(renderX, renderY, renderZ);
			anEntity.getOBJloader().DrawModel(gl,anEntity.getOrientation()*180.f/3.14159f);
			
			if(anEntity instanceof Enemy && ((Enemy)anEntity).getLife() == 0)
			{
				gl.glEnable (GL2.GL_BLEND);
				gl.glBlendFunc (GL2.GL_ONE, GL2.GL_ONE);
				gl.glColor4f(0f, 0f, 0f, 0.8f);
				death.DrawModel(gl, 0f);
				gl.glDisable(GL2.GL_BLEND);
			}
			
			gl.glTranslatef(-renderX, -renderY, -renderZ);

			gl.glTranslatef(-50f, 0f, -50f);
			
			

		}

		// Draw the tower in construction
		Tower	buildingTower = Game.getBuildingTower();
		if (buildingTower != null)
		{
			//Change position for local X Y Z functions, render and re-adjust back to origin
			gl.glTranslatef((float)wcoord[0], 0f, (float)wcoord[2]);
			buildingTower.getOBJloader().DrawModel(gl, 0f);
			
			
			switch (buildingTower.getRange())
			{
			case 2:
				gl.glEnable (GL2.GL_BLEND);
				gl.glBlendFunc (GL2.GL_ONE, GL2.GL_ONE);
				gl.glColor4f(0f, 0f, 0f, 0.5f);
				range2.DrawModel(gl,0f);
				gl.glDisable(GL2.GL_BLEND);
				
				break;
			case 3:
				gl.glEnable (GL2.GL_BLEND);
				gl.glBlendFunc (GL2.GL_ONE, GL2.GL_ONE);
				gl.glColor4f(0f, 0f, 0f, 0.5f);
				range3.DrawModel(gl,0f);
				gl.glDisable(GL2.GL_BLEND);
				break;
			case 4:
				gl.glEnable (GL2.GL_BLEND);
				gl.glBlendFunc (GL2.GL_ONE, GL2.GL_ONE);
				gl.glColor4f(0f, 0f, 0f, 0.5f);
				range4.DrawModel(gl,0f);
				gl.glDisable(GL2.GL_BLEND);
				break;
			case 5:
				gl.glEnable (GL2.GL_BLEND);
				gl.glBlendFunc (GL2.GL_ONE, GL2.GL_ONE);
				gl.glColor4f(0f, 0f, 0f, 0.5f);
				range5.DrawModel(gl,0f);
				gl.glDisable(GL2.GL_BLEND);
				break;
			default:
				break;
			}
			
			gl.glTranslatef(-(float)wcoord[0], 0f, -(float)wcoord[2]);
		}
		
		

		redraw();
	}

	/**
	 * redraw method call for repainting the display function
	 */
	public void redraw()
	{
		canvas.repaint();
	}


	/**
	 * Called when different display modes are changed (e.g. changing resolutions)
	 * @param gLDrawable
	 * @param modeChanged
	 * @param deviceChanged
	 */
	public void displayChanged(GLAutoDrawable gLDrawable, boolean modeChanged, boolean deviceChanged) 
	{
		System.out.println("displayChanged called");
	}


	/**
	 * Called when the window is resized to a size different than the prescribed initial window width and height
	 */
	public void reshape(GLAutoDrawable gLDrawable, int x, int y, int width, int height) 
	{

		System.out.println("reshape() called: x = "+x+", y = "+y+", width = "+width+", height = "+height);  

		// avoid a divide by zero error!
		if (height <= 0) 
		{
			height = 1;
		}

		final float h = (float) width / (float) height;

		gl.glViewport(0, 0, width, height);
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluPerspective(45.0f, h, 1.0, 240.0);
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();
	}

	
	
	/**
	 * This portion of the code does nothing, no OpenGL functionality has been created for this yet in Java
	 * This is used just as a place holder.
	*/
	public void dispose(GLAutoDrawable arg0) 
	{
		System.out.println("dispose() called");
	}




	//Begin Mouse Events
	/**
	 * Return the X and Y position of the canvas when a mouse is clicked
	 * as well as compute the 3D world space in X, Y and Z that the mouse
	 * is associated with.  This function requires a fixed Z-axis for selection.
	 */
	@Override
	public void mouseClicked(MouseEvent e) 
	{
		System.out.println("Mouse Clicked");


		realy = 0;
		objectX = e.getX();
		objectY = e.getY();

		System.out.println("x = "+objectX);
		System.out.println("y = "+objectY);


		/* note viewport[3] is height of window in pixels */
		realy = viewport[3] - (int) objectY - 1;
		System.out.println("viewport is: "+viewport[3]+"  and objectY is: "+objectY);
		System.out.println("Coordinates at cursor are (" + objectX + ", " + realy+")");


		//projection matrix algorithm
		// TODO I need to figure out a way to compute that Z-axis for depth of Y
		glu.gluUnProject((double) objectX, (double) realy, .9975, mvmatrix, 0, projmatrix, 0, viewport, 0, wcoord, 0);
		System.out.println("World coords at z = integral are (" + wcoord[0] + ", " + wcoord[1] + ", " + wcoord[2] + ")");



		float mapX = 11f - (float)(wcoord[2] + 50f) / 100f * 11f;
		float mapY = 11f - (float)(wcoord[0] + 50f) / 100f * 11f;
		Game.setMouseMapPosition(new MapElement((int)mapX, (int)mapY, 0));

		redraw();
	}


	/**
	 * Mouse enters frame.
	 */
	@Override
	public void mouseEntered(MouseEvent e) 
	{
		System.out.println("Mouse Entered Frame");
	}

	/**
	 * Mouse exits frame.
	 */
	@Override
	public void mouseExited(MouseEvent arg0) 
	{
		// TODO Auto-generated method stub
		System.out.println("Mouse Exited Frame");
	}

	/**
	 * Get positions X and Y in the 2D canvas plane and assign them to a new origin
	 * to be used later as the 'new' (0,0) for rotation and selection.
	 */
	@Override
	public void mousePressed(MouseEvent e) 
	{
		//Grab origin points for rotation
		System.out.println("Mouse is pressed.");
		getOriginX = e.getX();
		getOriginY = e.getY();
	}


	/**
	 * Mouse is released.
	 */
	@Override
	public void mouseReleased(MouseEvent e) 
	{
		//Grab last points of click and/drag on the map
		System.out.println("Mouse has been released");
		lastX = x;
		lastY = y;
	}


	/**
	 * Calculate the origin of rotation in both X and Y, even if the origin
	 * updates at a later time and redraw results of the transformations.
	 */
	@Override
	public void mouseDragged(MouseEvent e) 
	{
		System.out.println("Mouse is being dragged");


		//Set rotation origin = 0 as well as grabbing the last point on the map
		x =  (e.getX()-getOriginX)+lastX;//650
		y =  (e.getY()-getOriginY)+lastY;//300

		//for the first click, you want to grab x = 0, y = 0 as your start point
		//for every click thereafter, subtract your location in the X, Y plane as a start point and add/subtract
		System.out.println("x = "+x);
		System.out.println("y = "+y);

		redraw();	
	}


	/**
	 * Calculate the mouse movements in 3D space using a fixed Z-axis.
	 * This function is used for placing objects on the playing field
	 */
	@Override
	public void mouseMoved(MouseEvent e) 
	{
		realy = 0;
		objectX = e.getX();
		objectY = e.getY();

		System.out.println("x = "+objectX);
		System.out.println("y = "+objectY);


		/* note viewport[3] is height of window in pixels */
		realy = viewport[3] - (int) objectY - 1;
		
	    System.out.println("viewport is: "+viewport[3]+"  and objectY is: "+objectY);
	    System.out.println("Coordinates at cursor are (" + objectX + ", " + realy+")");
	   
	   
	   
	   //projection matrix algorithm
	   glu.gluUnProject((double) objectX, (double) realy, .9975, mvmatrix, 0, projmatrix, 0, viewport, 0, wcoord, 0);
	   System.out.println("World coords at z = integral are (" + wcoord[0] + ", " + wcoord[1] + ", " + wcoord[2] + ")");
	   
		redraw();


	}

	/**
	 * Press Enter to change from 'placement mode' to 'playing mode'
	 * Placement Mode - Changes to a 'top view' made for easy tower placement
	 * Playing Mode - Changes/Reverts back to standard perspective view for rotation
	 */
	@Override
	public void keyPressed(KeyEvent key) 
		  {
			    switch (key.getKeyChar()) {
			      case KeyEvent.VK_ENTER:
			      if(y == 75 && x ==0)
			      {
			    	  y = 1;
			    	  x = 1;
			      }
			      else
			      {
			        y = 75;
			        lastY = 75;
			        x = 0;
			        lastX = 0;
			        
			      }
			        
			        break;

			      default:
			        break;  
			    }
			    redraw();
			  }

	/**
	 * Release key on the keyboard
	 */
	@Override
	public void keyReleased(KeyEvent arg0) 
	{
		// TODO Auto-generated method stub	
	}

	/**
	 * Type key on the keyboard
	 */
	@Override
	public void keyTyped(KeyEvent key) 
	{
		// TODO Auto-generated method stub
	}
}
