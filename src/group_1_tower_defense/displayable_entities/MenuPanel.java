package group_1_tower_defense.displayable_entities;

import group_1_tower_defense.Displayable;
import group_1_tower_defense.Game;
import group_1_tower_defense.game_entities.Projectile;
import group_1_tower_defense.game_entities.Tower;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ToolTipManager;

public class MenuPanel extends JPanel
{
	public static int menuSideWidth = 250;
	public static int menuSideHeight = Displayable.windowHeight;
	public static List<Icons> allIcons = new ArrayList<Icons>();

	public JPanel menuRight = new JPanel(new FlowLayout(FlowLayout.RIGHT));
	public JPanel towerIcons;
	public JPanel towerParams;
	public JPanel gameVariables;

	public MenuPanel()
	{
		menuRight.setAlignmentY(RIGHT_ALIGNMENT);
		menuRight.setSize(new Dimension(menuSideWidth, Displayable.windowHeight));
		menuRight.setLayout(new GridLayout(3, 1));
		initWritten();
		ToolTipManager.sharedInstance().setInitialDelay(0);
		menuRight.add((gameVariables = initVariables()));
		menuRight.add((towerIcons = manageIcons()));
		menuRight.add((towerParams = initTowerPanel()));
		menuRight.remove(towerParams);
	}

	public void displayTowerVariables(final Tower selected)
	{
		if (selected == null)
		{
			menuRight.remove(towerParams);
			menuRight.repaint();
			return;
		}
		String tooltip="";
		if(selected.getLevelup() >= 0)
		{
			Tower up = Tower.getTowerTypes().get(selected.getLevelup());
			int price = up.getPrice();
			int damage = Projectile.getProjectileTypes().get(up.getProjectileID()).getDamage();
			int slow = Projectile.getProjectileTypes().get(up.getProjectileID()).getSlowEffect();
			String slowMsg = slow > 5 ? "Strong slow effect" : "Weak slow effect";
			float aoe = Projectile.getProjectileTypes().get(up.getProjectileID()).getAreaEffect();
			int addedDamage = (Projectile.getProjectileTypes().get(up.getProjectileID()).getDamage() - Projectile.getProjectileTypes().get(selected.getProjectileID()).getDamage());
			int range = up.getRange();
			int addedRange = (range - selected.getRange());
			tooltip ="<html><font size=5>Price : " + price + "<br/>Damage : " + damage + " (+" + addedDamage + ")<br/>" +
						"Range : " + range + " (+" + addedRange + ")<br/>" +
						(slow > 1 ? slowMsg + "(" + slow + ")<br/>" : "") + 
						(aoe > 0 ? "Aoe effect<br/>" : "") + "</font></html>";
		}
		menuRight.add(towerParams);
		towerParams.remove(sellTower);
		towerParams.remove(vUpgrade);
		sellTower = new JButton("Sell Tower");
		vUpgrade = new JButton("Up");
		vUpgrade.setVisible(false);
		dUpgrade.setVisible(false);
		lbUpgrade.setVisible(false);
		lbPrice.setVisible(true);
		vPrice.setVisible(true);
		sellTower.setVisible(false);
		if (Game.getSelectedTower() != null)
		{
			towerParams.remove(dUpgrade);
			vDamage.setText(Integer.toString(Projectile.getProjectileTypes().get(selected.getProjectileID()).getDamage()));
			vName.setText(selected.getName());
			vRange.setText(Integer.toString(selected.getRange()));
			//bUpgrade.addActionListener(this);
			if (selected.getLevelup() != -1)
			{
				lbUpgrade.setVisible(true);
				lbPrice.setText("Upgrade Price : ");
				vPrice.setText(Integer.toString(selected.getIncreaseLevelPrice()));
				vUpgrade.setVisible(true);
				vUpgrade.setText(Tower.getTowerTypes().get(selected.getLevelup()).getName());
				vUpgrade.setForeground(Color.red);
				vUpgrade.setFont(new Font("Dialog", Font.BOLD, 15));
				vUpgrade.setToolTipText(tooltip);
				towerParams.add(vUpgrade);
				vUpgrade.addMouseListener(new MouseAdapter(){
					public void mouseClicked(MouseEvent e) {
						Game.increaseTower(selected);
					}
				});
			}
			else
			{
				vPrice.setVisible(false);
				lbPrice.setVisible(false);
				vUpgrade.setVisible(false);
			}
			sellTower.setVisible(true);
			sellTower.setToolTipText("<html><font size='5'>(+" + (int) (selected.getPrice() * 0.8) + " credits)</font></html>");
			sellTower.addMouseListener(new MouseAdapter(){
				public void mouseClicked(MouseEvent e) {
					Game.destroyTower(selected);
					Game.increaseMoneyPoints((int) (selected.getPrice() * 0.8));
					Tower test = null;
					Game.setSelectedTower(test);
				}
			});
			towerParams.add(sellTower);
			menuRight.repaint();
		}
		else if (Game.getBuildingTower() != null)
		{
			sellTower.setVisible(false);
			vDamage.setText(Integer.toString(Projectile.getProjectileTypes().get(selected.getProjectileID()).getDamage()));
			vName.setText(selected.getName());
			vRange.setText(Integer.toString(selected.getRange()));
			vPrice.setText(Integer.toString(selected.getPrice()));
			if (selected.getLevelup() >= 0)
			{
				lbUpgrade.setVisible(true);
				dUpgrade.setVisible(true);
			dUpgrade.setText(Tower.getTowerTypes().get(selected.getLevelup()).getName());
			dUpgrade.setToolTipText(tooltip);
			towerParams.add(dUpgrade);
			}
		}
		menuRight.repaint();
		String specials = "";
	}

	public void refreshGameVariables()
	{
		vMoney.setText(Integer.toString(Game.getMoneyPoints()));
		vLife.setText(Integer.toString(Game.getLifePoints()));
		if (Game.getCurrentLevel() >= 0)
			vLevel.setText(Game.getLevels()[Game.getCurrentLevel()].name);
		vScore.setText(Integer.toString(Game.getScorePoints()));
	}

	public JPanel initVariables()
	{
		JPanel variablesPanel = new JPanel();
		variablesPanel.setLayout(new GridLayout(4, 2));
		//variablesPanel.setBackground(Color.gray);

		variablesPanel.add(lbScore);
		variablesPanel.add(lbLife);
		variablesPanel.add(vScore);
		variablesPanel.add(vLife);
		variablesPanel.add(lbLevel);
		variablesPanel.add(lbMoney);
		variablesPanel.add(vLevel);
		variablesPanel.add(vMoney);	


		return variablesPanel;
	}


	public JPanel initTowerPanel()
	{
		JPanel towerParam = new JPanel();
		towerParam.setLayout(new GridLayout(8, 1));
		//towerParam.setBackground(Color.GRAY);
		towerParam.add(lbName);
		towerParam.add(vName);
		towerParam.add(lbPrice);
		towerParam.add(vPrice);
		towerParam.add(lbDamage);
		towerParam.add(vDamage);
		towerParam.add(lbRange);
		towerParam.add(vRange);
		towerParam.add(lbUpgrade);
		towerParam.add(vUpgrade);
		towerParam.add(dUpgrade);
		towerParam.add(sellTower);
		return towerParam;
	}

	public JPanel manageIcons()
	{

		JPanel towerIcons = new JPanel();
		GridLayout test = new GridLayout(3, 3);
		towerIcons.setLayout(test);
		int i = 0;
		for (Tower aTower : Tower.getBuildableTowers())
		{
			towerIcons.add(aTower.getIcons().getIconLabel());
			int price = aTower.getPrice();
			int damage = Projectile.getProjectileTypes().get(aTower.getProjectileID()).getDamage();
			int slow = Projectile.getProjectileTypes().get(aTower.getProjectileID()).getSlowEffect();
			String slowMsg = slow > 5 ? "Strong slow effect" : "Weak slow effect";
			float aoe = Projectile.getProjectileTypes().get(aTower.getProjectileID()).getAreaEffect();
			int range = aTower.getRange();
			String tooltip ="<html><font size=4>Price : " + price + "<br/>Damage : " + damage + "<br/>" +
						"Range : " + range + "<br/>" +
						(slow > 1 ? slowMsg + "(" + slow + ")<br/>" : "") + 
						(aoe > 0 ? "Aoe effect<br/>" : "") + "</font></html>";
			aTower.getIcons().getIconLabel().setToolTipText(tooltip);
			i++;
		}
		for (;i < 9;i++)
			towerIcons.add(new JLabel(""));
		return towerIcons;
	}
	public void initWritten()
	{
		Font f = new Font("Dialog", Font.BOLD, 20);
		Font v = new Font("Dialog", Font.HANGING_BASELINE, 20);

		vLevel.setForeground(Color.GRAY);		
		vScore.setForeground(Color.GRAY);
		vLife.setForeground(Color.GRAY);
		vMoney.setForeground(Color.GRAY);
		dUpgrade.setForeground(Color.blue);

		lbLevel.setFont(f);
		lbMoney.setFont(f);
		lbScore.setFont(f);
		lbLife.setFont(f);

		vLevel.setFont(v);
		vScore.setFont(v);
		vLife.setFont(v);
		vMoney.setFont(v);

		Font g = new Font("Dialog", Font.BOLD, 15);
		Font h = new Font("Dialog", Font.HANGING_BASELINE, 16);

		lbName.setFont(g);
		lbPrice.setFont(g);
		lbDamage.setFont(g);
		lbRange.setFont(g);
		lbUpgrade.setFont(g);

		vName.setFont(h);
		vPrice.setFont(h);
		vDamage.setFont(h);
		vRange.setFont(h);
		vUpgrade.setFont(h);
		dUpgrade.setFont(h);
		sellTower.setFont(h);
	}


	public JLabel vName = new JLabel("", JLabel.LEFT);
	public JLabel vPosition = new JLabel("", JLabel.LEFT);
	public JLabel vLevel = new JLabel("", JLabel.CENTER);
	public JLabel vMoney = new JLabel("", JLabel.CENTER);
	public JLabel vScore = new JLabel("", JLabel.CENTER);
	public JLabel vLife = new JLabel("", JLabel.CENTER);
	public JLabel vDamage = new JLabel("", JLabel.LEFT);
	public JLabel vPrice = new JLabel("", JLabel.LEFT);
	public JLabel vRange = new JLabel("", JLabel.LEFT);
	public JLabel dUpgrade = new JLabel("", JLabel.LEFT);

	public JButton vUpgrade = new JButton("Upgrade");
	public JButton sellTower = new JButton("Sell Tower");
	private JLabel lbName = new JLabel("Name :", JLabel.CENTER);
	private JLabel lbLevel= new JLabel("LEVEL", JLabel.CENTER);
	private JLabel lbMoney= new JLabel("MONEY", JLabel.CENTER);
	private JLabel lbScore= new JLabel("SCORE", JLabel.CENTER);
	private JLabel lbLife= new JLabel("LIFES", JLabel.CENTER);
	private JLabel lbDamage= new JLabel("Damage :", JLabel.CENTER);
	private JLabel lbPrice= new JLabel("Price :", JLabel.CENTER);
	private JLabel lbRange= new JLabel("Range :", JLabel.CENTER);
	private JLabel lbUpgrade= new JLabel("Upgrade :", JLabel.CENTER);
}
