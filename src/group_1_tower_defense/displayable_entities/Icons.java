package group_1_tower_defense.displayable_entities;

import group_1_tower_defense.Game;
import group_1_tower_defense.game_entities.Projectile;
import group_1_tower_defense.game_entities.Tower;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Icons
{
	private ImageIcon normalImage = null;
	private ImageIcon selectedImage = null;
	private int objectId = -1;
	private boolean isSelected = false;
	private JLabel iconLabel = null;

	public Icons(ImageIcon ni, ImageIcon si, int objectId, boolean isSelected)
	{
		this.normalImage = ni;
		this.selectedImage = si;
		this.objectId = objectId;
		loadIcon();
	}
	public Icons(String normalImage, String selectedImage, int id)
	{
		this.setNormalImage(new ImageIcon(normalImage));
		if (selectedImage != null)
		this.selectedImage = new ImageIcon(selectedImage);
		else
			this.selectedImage = this.getNormalImage();
		this.setObjectId(id);
		loadIcon();
	}

	public void hideAllIcons()
	{
		for (int i = 0; i < MenuPanel.allIcons.size(); i++)
				MenuPanel.allIcons.get(i).setSelected(false);
	}
	
	public void loadIcon()
	{
		MenuPanel.allIcons.add(this);
		setIconLabel(new JLabel(this.getNormalImage()));
		getIconLabel().addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e) {
				hideAllIcons();
				setSelected(true);
				System.out.print("clickID[" + getObjectId() + "] at: " + e.getPoint() +"\n");
			}
		});
		System.out.print("Icon of object[" + getObjectId() + "]added\n");
	}

	public boolean isSelected()
	{
		return isSelected;
	}

	public void setSelected(boolean isSelected)
	{
		this.isSelected = isSelected;
		if (isSelected == true)
		{
			Game.setBuildingTower(Tower.getTowerTypes().get(objectId));
			getIconLabel().setIcon(selectedImage);
		}
		else
			getIconLabel().setIcon(getNormalImage());
	}

	public JLabel getIconLabel()
	{
		return iconLabel;
	}

	public void setIconLabel(JLabel iconLabel)
	{
		this.iconLabel = iconLabel;
	}

	public int getObjectId()
	{
		return objectId;
	}

	public void setObjectId(int objectId)
	{
		this.objectId = objectId;
	}

	public ImageIcon getNormalImage()
	{
		return normalImage;
	}

	public void setNormalImage(ImageIcon normalImage)
	{
		this.normalImage = normalImage;
	}
}
