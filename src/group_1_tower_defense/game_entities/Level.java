/**
 * 
 */
package group_1_tower_defense.game_entities;

/**
 * @author Alastra
 *
 */
public class Level
{
	/**
	 * PopEnemies contains the informations to add a defined number of a specific type of Enemy
	 */
	public class PopEnemies
	{
		/**
		 * The ID of the type of Enemy to pop for this level
		 */
		public int	enemiesID;
		
		/**
		 * The number of Enemy to pop for this level
		 */
		public int	enemiesNumber;
	}

	/**
	 * time to wait before starting this level (in ms)
	 */
	public int			timeBeforeStarting;
	
	/**
	 * name of this level
	 */
	public String			name;
	
	/**
	 * an array of PopEnemies representing all the enemies that have to be pop for this Level
	 */
	public PopEnemies	enemiesToPop[];
}
