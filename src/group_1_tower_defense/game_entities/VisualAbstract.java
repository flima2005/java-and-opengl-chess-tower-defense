package group_1_tower_defense.game_entities;

import group_1_tower_defense.displayable_entities.OBJloader;


/**
 * 
 * @author Maxime Thibaut, 
 * This class has to contains the visual settings needed by openGL
 */
public abstract class VisualAbstract
{
	/**
	 * the id of the image in the factory
	 */
	protected int			visualID;
	
	/**
	 * get the relative OBJ loader and texture file
	 */
	public OBJloader		getOBJloader()
	{
		return OBJFactory.getSingleton().getOBJloader(this.getClass().getName(), visualID);
	}
}
