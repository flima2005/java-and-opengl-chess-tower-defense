package group_1_tower_defense.game_entities;

import java.util.ArrayList;
import java.util.List;

import group_1_tower_defense.Game;

/**
 * 
 * @author Maxime Thibaut
 *
 * The Projectile class will be it's own Factory.
 * It means that the constructors of Projectile will be private and you will have to use a public static method to instantiate a new one.
 *
 */
public class Projectile extends MapEntityAbstract
{
/*
 * FACTORY PATTERN
 */
	/**
	 * private default constructor (factory pattern)
	 */
	private Projectile()
	{
	}

	/**
	 * private copy constructor (factory pattern)
	 */
	private Projectile(Projectile aProjectile)
	{
		visualID	= aProjectile.visualID;
		type		= aProjectile.type;
		damage		= aProjectile.damage;
		speed		= aProjectile.speed;
		areaEffect	= aProjectile.areaEffect;
		slowEffect	= aProjectile.slowEffect;
		target		= null;
	}

	/**
	 * array of types of towers
	 */
	private static	ArrayList<Projectile> projectileTypes;
	
	/**
	 * add a new pre-defined object
	 * @return id of the new type of tower
	 */
	public static int	newTypeOfProjectile(int newVisualID, int newDamage, int newSpeed, int newArea, int newSlow)
	{
		if (projectileTypes == null) // allocate
			projectileTypes = new ArrayList<Projectile>();
		Projectile	newProjectile = new Projectile();
		newProjectile.visualID = newVisualID;
		newProjectile.type = projectileTypes.size();
		newProjectile.damage = newDamage;
		newProjectile.speed = newSpeed;
		newProjectile.areaEffect = newArea;
		newProjectile.slowEffect = newSlow;
		projectileTypes.add(newProjectile.type, newProjectile);
		return newProjectile.type;
	}
	
	/**
	 * add a new pre-defined object
	 */
	public static Projectile	getNewProjectile(int projectileID)
	{
		if (0 <= projectileID && projectileID < projectileTypes.size())
		{
			return new Projectile(projectileTypes.get(projectileID));
		}
		else
		{
			System.err.println("Error: trying to instantiate a non existant type of tower.");
			return null;
		}
	}

/*
 * VARIABLES
 */
	/**
	 * type of the object (id)
	 */
	protected int	type;
	
	/**
	 * damage
	 */
	protected int	damage;
	
	/**
	 * speed of the projectile [0 ; x]
	 * if 0, the projectile instant hit the target
	 */
	protected int	speed;
	
	/**
	 * area of damage [0 ; x]
	 * if 0, no area effect, only the target is hit
	 */
	protected float	areaEffect;
	
	/**
	 * slow effect caused to the enemy(ies)
	 * the slow value is the divider of the hit enemy speed
	 */
	protected int	slowEffect;
	
	/**
	 * actual target (ref to a Enemy or null if no specific target)
	 */
	protected Enemy	target;
	
/*
 * SETTER AND GETTER
 */
	/**
	 * @return the projectileTypes
	 */
	public static ArrayList<Projectile> getProjectileTypes() {
		return projectileTypes;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @return the damage
	 */
	public int getDamage() {
		return damage;
	}

	/**
	 * @return the speed
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * @return the areaEffect
	 */
	public float getAreaEffect() {
		return areaEffect;
	}

	/**
	 * @return the slowEffect
	 */
	public int getSlowEffect() {
		return slowEffect;
	}

	/**
	 * @return the target
	 */
	public Enemy getTarget() {
		return target;
	}

	/**
	 * @param target the target to set
	 */
	public void setTarget(Enemy target) {
		this.target = target;
	}
	
/*
 * METHODS
 */
	/**
	 * Depending on the projectile speed, move it to its target and check collision or immediately impact the enemy
	 */
	public void	move()
	{
		// move the Projectile
		float		moveX = target.getX() - x;
		float		moveY = target.getY() - y;
		float		moveNorme = (float)Math.sqrt(moveX * moveX + moveY * moveY);
		moveX /= moveNorme;
		moveY /= moveNorme;
		moveX *= (float)speed / Game.getMap().getSquareSize();
		moveY *= (float)speed / Game.getMap().getSquareSize();
		x += moveX;
		y += moveY;
		// check if it has approximately reached the target position and hit it
		if (speed <= 0 || (target.getX() - 0.25f <= x && x <= target.getX() + 0.25f && target.getY() - 0.25f <= y && y <= target.getY() + 0.25f))
			hitTarget();
	}
	
	/**
	 * decrease the target's life, apply effects and delete projectile
	 */
	private void	hitTarget()
	{
		target.decreaseLife(damage);
		if (slowEffect >= 1)
			target.setSlowEffect(slowEffect);
		if (areaEffect > 0)
			hitInRangeEnemies(Game.getEnemies());
		Game.addProjectileToDelete(this); // FIXME avoid strong dependencies between other class by calling Game. Game should call instead.
	}

	public void		hitInRangeEnemies(List<Enemy> enemies)
	{
		for (Enemy anEnemy : enemies)
		{
			float vecX = anEnemy.getX() - x;
			float vecY = anEnemy.getY() - y;
			float vecNorm = vecX * vecX + vecY * vecY;
			float areaNorm = areaEffect * areaEffect;
			if (vecNorm < areaNorm)
			{
				float areaDamage = (1.f - vecNorm / areaNorm) * damage;
				anEnemy.decreaseLife((int)areaDamage);
			}
		}
	}
}
