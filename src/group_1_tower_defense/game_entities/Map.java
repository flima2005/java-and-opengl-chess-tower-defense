package group_1_tower_defense.game_entities;

/**
 * @author Guillaume Lebedel
 * 
 * This class represent the full grid.
 *
 */
public class Map
{
	/**
	 * Store all the squares of the map (position + type)
	 * 2 dimensional array [y][x]
	 */
	private MapElement[/* y */][/* x */] grid;
	
	/**
	 * width of the map
	 */
	private int sizeX = 0;
	
	/**
	 * height of the map
	 */
	private int sizeY = 0;
	
	/**
	 * real drawing size of a square
	 */
	private int squareSize = 0;
	
	public Map(int sizeX, int sizeY, MapElement[][] grid)
	{
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.grid = grid;
	}
	
	public MapElement[][] getGrid()
	{
		return grid;
	}
	public void setGrid(MapElement[][] grid)
	{
		this.grid = grid;
	}
	public int getSizeY()
	{
		return sizeY;
	}
	public void setSizeY(int sizeY)
	{
		this.sizeY = sizeY;
	}
	public int getSizeX()
	{
		return sizeX;
	}
	public void setSizeX(int sizeX)
	{
		this.sizeX = sizeX;
	}

	public int getSquareSize()
	{
		return squareSize;
	}

	public void setSquareSize(int squareSize)
	{
		this.squareSize = squareSize;
	}
	
	public MapElement	getMapElement(int x, int y)
	{
		if (grid == null)
			return null;
		return grid[y][x];
	}
	
	public void	setMapElement(int x, int y, int type)
	{
		grid[y][x].setType(type);
	}
	
	public String toString()
	{
		String value = new String();
		value = value + "MAP = \n";
		for (int y = 0; y < sizeY; y++)
		{
			for (int x = 0; x < sizeX; x++)
				value = value + getMapElement(x, y).getType();
			if (y + 1 < sizeY)
				value = value + "\n";
		}
		return value;
	}
	
	public void print()
	{
		System.out.println(toString());
	}
}
