package group_1_tower_defense.game_entities;

/**
 * 
 * @author Maxime Thibaut
 *
 */
public abstract class MapEntityAbstract extends VisualAbstract {
	/**
	 * x position of the object on the map
	 */
	protected float	x;

	/**
	 * y position of the object on the map
	 */
	protected float	y;

	/**
	 * orientation of the object on the map (radius)
	 */
	protected float	orientation;

/*
 * GETTER AND SETTER
 */
	/**
	 * @return the x
	 */
	public float getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(float x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public float getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(float y) {
		this.y = y;
	}

	/**
	 * @return the orientation
	 */
	public float getOrientation() {
		return orientation;
	}

	/**
	 * @param orientation the orientation to set
	 */
	public void setOrientation(float orientation) {
		this.orientation = orientation;
	}
}
