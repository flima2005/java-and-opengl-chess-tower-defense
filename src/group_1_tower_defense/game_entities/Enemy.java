package group_1_tower_defense.game_entities;

import java.util.ArrayList;

import group_1_tower_defense.Audio;
import group_1_tower_defense.Game;

/**
 * 
 * @author Maxime Thibaut
 *
 * The Enemy class will be it's own Factory.
 * It means that the constructors of Enemy will be private and you will have to use a public static method to instantiate a new one.
 *
 */
public class Enemy extends MapEntityAbstract
{
/*
 * FACTORY PATTERN
 */
	/**
	 * private default constructor (factory pattern)
	 */
	private Enemy()
	{
	}

	/**
	 * private copy constructor (factory pattern)
	 */
	private Enemy(Enemy anEnemy)
	{
		visualID	= anEnemy.visualID;
		type		= anEnemy.type;
		life		= anEnemy.life;
		speed		= anEnemy.speed;
		damage		= anEnemy.damage;
		destination	= anEnemy.destination;
		isFlying	= anEnemy.isFlying;
		givenMoney	= anEnemy.givenMoney;
	}
	
	/**
	 * array of types of towers
	 */
	private static ArrayList<Enemy>	enemyTypes;
	
	/**
	 * add a new pre-defined object
	 * @return id of the new type of tower
	 */
	public static int	newTypeOfEnemy(int newVisualID, int newLife, int newSpeed, int newDamage, boolean flies, int givenMoney)
	{
		if (enemyTypes == null) // allocate
			enemyTypes = new ArrayList<Enemy>();
		Enemy	newEnemy = new Enemy();
		newEnemy.visualID = newVisualID;
		newEnemy.type = enemyTypes.size();
		newEnemy.life = newLife;
		newEnemy.speed = newSpeed;
		newEnemy.damage = newDamage;
		newEnemy.isFlying = flies;
		newEnemy.givenMoney = givenMoney;
		enemyTypes.add(newEnemy.type, newEnemy);
		return enemyTypes.size() - 1;
	}
	
	/**
	 * add a new pre-defined object
	 */
	public static Enemy	getNewEnemy(int enemyID)
	{
		if (0 <= enemyID && enemyID < enemyTypes.size())
		{
			return new Enemy(enemyTypes.get(enemyID));
		}
		else
		{
			System.err.println("Error: trying to instantiate a non existant type of Enemy.");
			return null;
		}
	}

/*
 * VARIABLES
 */
	/**
	 * type of the object (id)
	 */
	protected int	type;
	
	/**
	 * life of the enemy
	 */
	protected int	life;
	
	/**
	 * speed of the enemy
	 */
	protected int	speed;
	
	/**
	 * divider of the speed set by the last projectile
	 * decrease by 1 until == 1 after each move
	 */
	protected float	slowEffect;

	/**
	 * each move decrease the slowEffect by this value
	 */
	protected static final float	SLOW_EFFECT_DECREASER = 0.04f;
	
	/**
	 * damage of the enemy
	 */
	protected int	damage;
	
	/**
	 * next position to reach
	 */
	protected MapElement	destination;

	/**
	 * flying Enemy or not
	 */
	protected boolean isFlying;
	
	/**
	 * the money that the player will earn when this Enemy will die
	 */
	protected int	givenMoney;

/*
 * GETTER AND SETTER
 */
	/**
	 * @return the enemyTypes
	 */
	public static ArrayList<Enemy> getEnemyTypes() {
		return enemyTypes;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @return the life
	 */
	public int getLife() {
		return life;
	}

	/**
	 * @param lifeToLess the life to decrease from the actual
	 */
	public void decreaseLife(int lifeToLess) {
		this.life -= lifeToLess;
		if (this.life <= 0)
		{
			Game.addEnemyToDelete(this);
			new Audio("assets/death.wav").start();
			System.out.println("Enemy killed !");
		}
	}

	/**
	 * @return the speed
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * @return the damage
	 */
	public int getDamage() {
		return damage;
	}
	
	/**
	 * @return the destination
	 */
	public MapElement getDestination() {
		return destination;
	}

	/**
	 * @param destination the destination to set
	 */
	public void setDestination(MapElement destination) {
		this.destination = destination;
	}

	/**
	 * @return the destination
	 */
	public MapElement destination()
	{
		return destination;
	}

	/**
	 * @return the flying flag
	 */
	public boolean isFlying()
	{
		return isFlying;
	}
	
	/**
	 * @return the givenMoney
	 */
	public int getGivenMoney() {
		return givenMoney;
	}
	
	/**
	 * @return the slowEffect
	 */
	public float getSlowEffect() {
		return slowEffect;
	}

	/**
	 * @param slowEffect the slowEffect to set
	 */
	public void setSlowEffect(float slowEffect) {
		if (slowEffect > this.slowEffect)
			this.slowEffect = slowEffect;
	}

/*
 * METHODS
 */
	/**
	 * modify the enemy position using the nearest cell it has to go and its speed
	 */
	public void		move()
	{
		// check the slowEffect value
		if (slowEffect < 1)
			slowEffect = 1;
		// move the Enemy
		float		moveX = (float)(destination.getX() + 0.5f) - x;
		float		moveY = (float)(destination.getY() + 0.5f) - y;
		float		moveNorme = (float)Math.sqrt(moveX * moveX + moveY * moveY);
		moveX /= moveNorme;
		moveY /= moveNorme;
		moveX *= ((float)speed / slowEffect) / (float)Game.getMap().getSquareSize();
		moveY *= ((float)speed / slowEffect) / (float)Game.getMap().getSquareSize();
		moveNorme = (float)Math.sqrt(moveX * moveX + moveY * moveY);
		x += moveX;
		y += moveY;
		// decrease the slow effect
		if (slowEffect > 1)
			slowEffect -= SLOW_EFFECT_DECREASER;
		// move vector compared to the unit vector u(1, 0) using dot product
		// the dot product is equal to moveX in fact
		float		cossOrientation = moveX / moveNorme;
		orientation = (float)Math.acos(cossOrientation);
		// check if it has reached an endpoint
		if (destination.getType() == MapElement.END_POINT
			&& (int)x == destination.getX()
			&& (int)y == destination.getY())
		{
			Game.decreaseLifePoints(damage);
			Game.addEnemyToDelete(this);
			System.out.println("Lost of " + damage + " points of life");
		}
	}
}
