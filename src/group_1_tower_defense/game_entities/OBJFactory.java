package group_1_tower_defense.game_entities;

import group_1_tower_defense.displayable_entities.OBJloader;

import java.util.ArrayList;

/**
 * 
 * @author Maxime Thibaut
 *
 * This singleton class contains all the loaded OBJloader to avoid multiple loading of a same image
 *
 */
public class OBJFactory
{
/*
 * SINGLETON PATTERN
 */
	/**
	 * singleton holder
	 */
	private static OBJFactory	singleton = null;
	
	/**
	 * default constructor
	 */
	private OBJFactory()
	{
		towerModels			= new ArrayList<OBJloader>();
		projectileModels	= new ArrayList<OBJloader>();
		enemyModels			= new ArrayList<OBJloader>();
		mapModels			= new ArrayList<OBJloader>();
	}

	/**
	 * copy constructor
	 */
	private OBJFactory(OBJFactory anImagesFactory)
	{
	}
	
	/**
	 * return the singleton
	 */
	public static OBJFactory	getSingleton()
	{
		if (singleton == null)
			singleton = new OBJFactory();
		return singleton;
	}

/*
 * FACTORY PATTERN	
 */
	/**
	 * towerModels contains the buffer containing all the vertices of the towers
	 */
	private ArrayList<OBJloader>	towerModels;
	
	/**
	 * projectileModels contains the buffer containing all the vertices of the projectiles
	 */
	private ArrayList<OBJloader>	projectileModels;
	
	/**
	 * enemyModels contains the buffer containing all the vertices of the enemies
	 */
	private ArrayList<OBJloader>	enemyModels;
	
	/**
	 * mapModels contains the buffer containing all the vertices of the map
	 */
	private ArrayList<OBJloader>	mapModels;
		
	/**
	 * load and set the associated OBJloader to the type and paths
	 */
	public void	setOBJloader(String type, int id, String modelPath, String texturePath)
	{
		// LOAD
		OBJloader	objl =  new OBJloader(modelPath, texturePath);
		
		// STORE
		if (type.equals(Tower.class.getSimpleName()))
			towerModels.add(id, objl);
		else if (type.equals(Projectile.class.getSimpleName()))
			projectileModels.add(id, objl);
		else if (type.equals(Enemy.class.getSimpleName()))
			enemyModels.add(id, objl);
		else if (type.equals(Map.class.getSimpleName()))
			mapModels.add(id, objl);
		else
			System.err.println("Error: tried to add a visual with an unknown type(" + type + ")");
	}
	
	/**
	 * return the associated texture to the type and id
	 */
	public OBJloader	getOBJloader(String type, int id)
	{
		OBJloader objl = null;
		if (type == Tower.class.getName() && 0 <= id && id < towerModels.size())
			objl = towerModels.get(id);
		else if (type == Projectile.class.getName() && 0 <= id && id < projectileModels.size())
			objl = projectileModels.get(id);
		else if (type == Enemy.class.getName() && 0 <= id && id < enemyModels.size())
			objl = enemyModels.get(id);
		else if (type == Map.class.getName() && 0 <= id && id < mapModels.size())
			objl = mapModels.get(id);
		else
			System.err.println("Error: tried to get a model with a wrong type or id");
		return objl;
	}
}
