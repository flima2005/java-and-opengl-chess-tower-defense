package group_1_tower_defense;

import group_1_tower_defense.game_entities.Enemy;
import group_1_tower_defense.game_entities.Level;
import group_1_tower_defense.game_entities.Level.PopEnemies;
import group_1_tower_defense.game_entities.Map;
import group_1_tower_defense.game_entities.MapElement;
import group_1_tower_defense.game_entities.MapEntityAbstract;
import group_1_tower_defense.game_entities.Projectile;
import group_1_tower_defense.game_entities.Tower;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

/**
 * 
 * @author Maxime Thibaut
 *
 */
public class Game
{
	/*========================================================================
	 *========================================================================
	 * 								VARIABLES
	 *========================================================================
	 *========================================================================*/
	/**
	 * frame duration (in ms)
	 * 1000 / 25 st 25fps
	 * 1000 / 60 st 60fps
	 */
	private static final long	FRAME_DURATION = 1000 / 25;

	/**
	 * Parser class to parse the .xml config file
	 */
	private static Parser		parser;

	/**
	 * PathFinder class to find the paths for each enemy trough the map
	 */
	private static PathFinder	pathFinder;

	/**
	 * Displayable class to render the game
	 */
	private static Displayable	displayable;

	/**
	 * map representing the game area
	 */
	private static Map			map;

	/**
	 * List of Tower
	 */
	private static List<Tower>	towers;

	/**
	 * List of Tower to delete
	 */
	private static List<Tower>	towersToDelete;
	
	/**
	 * ref to the current selected Tower
	 * null if no Tower obj is selected
	 */
	private static Tower		selectedTower;

	/**
	 * ref to the current building Tower
	 * null if no Tower obj is currently in construction
	 */
	private static Tower		buildingTower;
	
	/**
	 * if the mouse has been clicked on the map, this position will be set such that the gameLoop can handle it later
	 */
	private static MapElement	mouseMapPosition;

	/**
	 * List of Projectile
	 */
	private static List<Projectile>	projectiles;

	/**
	 * List of Projectile to delete
	 */
	private static List<Projectile>	projectilesToDelete;

	/**
	 * List of Enemy
	 */
	private static List<Enemy>	enemies;

	/**
	 * List of Enemy left to pop
	 */
	private static List<Enemy>	enemiesToPop;

	/**
	 * List of Enemy to delete
	 */
	private static List<Enemy>	enemiesToDelete;

	/**
	 * Array of Level (each Level contains the events)
	 */
	private static Level		levels[];

	/**
	 * the current level of the game 
	 */
	private static int			currentLevel;

	/**
	 * beginning of the next level (ms timestamp)
	 */
	private static long			timeToNextLevel;
	
	/**
	 * elapsing time between 2 Enemies poping at the same startpoint (in ms)
	 */
	private static final long	POP_ENEMIES_TIME_LAPSE = 1800;

	/**
	 * Life points left
	 */
	private static int			lifePoints;
	
	/**
	 * current amount of money
	 */
	private static int			moneyPoints;

	/**
	 * score of the game
	 */
	private static int			scorePoints;

	/*========================================================================
	 *========================================================================
	 * 								GETTER & SETTER
	 *========================================================================
	 *========================================================================*/
	/**
	 * @return the map
	 */
	public static Map getMap()
	{
		return map;
	}

	/**
	 * @param map the map to set
	 */
	public static void setMap(Map map)
	{
		Game.map = map;
	}

	/**
	 * @return the towers
	 */
	public static List<Tower> getTowers() {
		return towers;
	}

	/**
	 * @return the projectiles
	 */
	public static List<Projectile> getProjectiles() {
		return projectiles;
	}
	
	/**
	 * add a given Projectile in the projectiles list
	 * @param newProjectile the Projectile to add
	 */
	public static void	addProjectile(Projectile newProjectile)
	{
		projectiles.add(newProjectile);
	}

	/**
	 * @return the enemies
	 */
	public static List<Enemy> getEnemies() {
		return enemies;
	}

	public static List<MapEntityAbstract>	getDisplayableItems(){
		List<MapEntityAbstract>	displayableItems = new ArrayList<MapEntityAbstract>();
		displayableItems.addAll(towers);
		displayableItems.addAll(enemies);
		displayableItems.addAll(projectiles);
		return displayableItems;
	}
	
	/**
	 * @return the levels
	 */
	public static Level[] getLevels()
	{
		return levels;
	}

	/**
	 * @param levels the levels to set
	 */
	public static void setLevels(Level levels[])
	{
		Game.levels = levels;
	}

	/**
	 * @return the currentLevel
	 */
	public static int getCurrentLevel()
	{
		return currentLevel;
	}

	/**
	 * @return the lifePoints
	 */
	public static int getLifePoints() {
		return lifePoints;
	}

	/**
	 * @param lifePoints the lifePoints to set
	 */
	public static void setLifePoints(int lifePoints) {
		Game.lifePoints = lifePoints;
	}

	/**
	 * @param lifePointsToLess the lifePoints to less
	 */
	public static void decreaseLifePoints(int lifePointsToLess) {
		Game.lifePoints -= lifePointsToLess;
	}
	
	/**
	 * @param moneyPoints the moneyPoints to set
	 */
	public static void setMoneyPoints(int moneyPoints) {
		Game.moneyPoints = moneyPoints;
	}

	/**
	 * @return the moneyPoints
	 */
	public static int getMoneyPoints() {
		return moneyPoints;
	}

	/**
	 * @param increaseAmount the amount of money to add
	 */
	public static void increaseMoneyPoints(int increaseAmount) {
		Game.moneyPoints += increaseAmount;
	}

	/**
	 * @param decreaseAmount the amount of money to less
	 */
	public static void decreaseMoneyPoints(int decreaseAmount) {
		Game.moneyPoints -= decreaseAmount;
	}

	/**
	 * @return the scorePoints
	 */
	public static int getScorePoints() {
		return scorePoints;
	}
	
	/**
	 * @param increaseBy the amount to add to the actual scorePoints
	 */
	public static void	increaseScorePoints(int increaseBy) {
		Game.scorePoints += increaseBy;
	}

	/**
	 * @return the timeToNextLevel
	 */
	public static float getTimeLeftBeforeNextLevel() {
		return timeToNextLevel;
	}

	/**
	 * @return the towersToDelete
	 */
	public static List<Tower> getTowersToDelete() {
		return towersToDelete;
	}

	/**
	 * @param towerToDelete the Tower to delete
	 */
	public static void addTowersToDelete(Tower towerToDelete) {
		Game.towersToDelete.add(towerToDelete);
	}

	/**
	 * @return the projectilesToDelete
	 */
	public static List<Projectile> getProjectilesToDelete() {
		return projectilesToDelete;
	}

	/**
	 * @param projectileToDelete the projectile to delete
	 */
	public static void addProjectileToDelete(Projectile projectileToDelete) {
		Game.projectilesToDelete.add(projectileToDelete);
	}

	/**
	 * @return the enemiesToDelete
	 */
	public static List<Enemy> getEnemiesToDelete() {
		return enemiesToDelete;
	}

	/**
	 * @param enemyToDelete the enemy to delete
	 */
	public static void addEnemyToDelete(Enemy enemyToDelete) {
		Game.enemiesToDelete.add(enemyToDelete);
	}
	
	/**
	 * @return the buildingTower
	 */
	public static Tower getBuildingTower() {
		return buildingTower;
	}

	/**
	 * @param buildingTower the buildingTower to set
	 */
	public static void setBuildingTower(Tower buildingTower) {
		Game.selectedTower = null;
		Game.buildingTower = buildingTower;
		getDisplayable().mp.displayTowerVariables(buildingTower);
	}

	/**
	 * @return the selectedTower
	 */
	public static Tower getSelectedTower() {
		return selectedTower;
	}

	/**
	 * @param selectedTower the selectedTower to set
	 */
	public static void setSelectedTower(Tower selectedTower) {
		if (Game.buildingTower != null)
			Game.buildingTower.getIcons().setSelected(false);
		Game.buildingTower = null;
		Game.selectedTower = selectedTower;
		getDisplayable().mp.displayTowerVariables(selectedTower);
	}

	/**
	 * @param cell the position of the selectedTower to set
	 */
	public static void setSelectedTower(MapElement cell) {
		setSelectedTower(getTower(cell));
	}

	/**
	 * @return the displayable
	 */
	public static Displayable getDisplayable()
	{
		return displayable;
	}

	/**
	 * @param displayable the displayable
	 */
	public static void setDisplayable(Displayable displayable)
	{
		Game.displayable = displayable;
	}

	/**
	 * @param mouseMapPosition the mouseMapPosition to set by the Displayable when map is clicked
	 */
	public static void setMouseMapPosition(MapElement mouseMapPosition) {
		if (mouseMapPosition == null
			|| (0 <= mouseMapPosition.getX() && mouseMapPosition.getX() < map.getSizeX()
				&& 0 <= mouseMapPosition.getY() && mouseMapPosition.getY() < map.getSizeY()))
			Game.mouseMapPosition = mouseMapPosition;
		else
			setSelectedTower((Tower)null);
	}

	/*========================================================================
	 *========================================================================
	 * 								METHODS
	 *========================================================================
	 *========================================================================*/
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		// initialize the data structures and the objects
		init();

		// parse the xml file to fill all the data structures (4 factories, map, levels)
		parser.loadFile("config_files/settings.xml");

		// set the map to the PathFinder
		pathFinder.init(map);
		
		// init main window
		getDisplayable().init();
		
		// start the main loop
		gameLoop();
	}

	/**
	 * Init the data structure of the game
	 */
	private static void	init()
	{
		// default values
		map			= null;
		levels		= null;
		currentLevel = -1;
		timeToNextLevel = 0;
		lifePoints	= 100;
		moneyPoints = 10;
		scorePoints	= 0;

		// initialize the list of visible items and toDelete items
		towers				= new ArrayList<Tower>();
		towersToDelete		= new ArrayList<Tower>();
		projectiles			= new ArrayList<Projectile>();
		projectilesToDelete	= new ArrayList<Projectile>();
		enemies				= new ArrayList<Enemy>();
		enemiesToPop		= new ArrayList<Enemy>();
		enemiesToDelete		= new ArrayList<Enemy>();

		// instantiate the Parser
		parser		= new Parser();

		// instantiate the PathFinder and set it the map
		pathFinder	= new PathFinder();

		// instantiate the Displayable
		setDisplayable(new Displayable());
		mouseMapPosition = null;
	}

	/**
	 * game main loop in which all the game run
	 */
	private static void	gameLoop()
	{
//		// DEBUG remove the following lines
//		// damage towers
//		System.out.println("BUILD TOWER : " + buildNewTower(new MapElement(9, 3, MapElement.EMPTY_SQUARE), 0));
//		System.out.println("BUILD TOWER : " + buildNewTower(new MapElement(6, 6, MapElement.EMPTY_SQUARE), 0));
//		// slow towers
//		System.out.println("BUILD TOWER : " + buildNewTower(new MapElement(5, 2, MapElement.EMPTY_SQUARE), 3));
//		System.out.println("BUILD TOWER : " + buildNewTower(new MapElement(5, 3, MapElement.EMPTY_SQUARE), 3));
//		System.out.println("BUILD TOWER : " + buildNewTower(new MapElement(5, 4, MapElement.EMPTY_SQUARE), 3));
//		System.out.println("BUILD TOWER : " + buildNewTower(new MapElement(5, 5, MapElement.EMPTY_SQUARE), 3));
//		System.out.println("BUILD TOWER : " + buildNewTower(new MapElement(5, 6, MapElement.EMPTY_SQUARE), 3));
//		System.out.println("BUILD TOWER : " + buildNewTower(new MapElement(5, 7, MapElement.EMPTY_SQUARE), 3));
//		System.out.println("BUILD TOWER : " + buildNewTower(new MapElement(5, 8, MapElement.EMPTY_SQUARE), 3));
//		System.out.println("BUILD TOWER : " + buildNewTower(new MapElement(5, 9, MapElement.EMPTY_SQUARE), 3));

		while (lifePoints > 0)
		{
			long	currentTime = System.currentTimeMillis();
			long	executingLoopTime = currentTime;
			getDisplayable().mp.refreshGameVariables();
			// if the enemies list is empty, start the next level
			if (enemies.size() == 0 && enemiesToPop.size() == 0)
			{
				currentLevel++;
				if (currentLevel == levels.length)
					break;
				// set the timer to the time to wait before launching the new level
				timeToNextLevel = currentTime + levels[currentLevel].timeBeforeStarting;
				// set all the enemies that we will need to pop for this level
				fillEnemiesToPop(levels[currentLevel].enemiesToPop);
				System.out.println("---------- New Level (" + currentLevel + " : " + levels[currentLevel].name + "): " + enemiesToPop.size() + " enemies to pop");
			}
			// when the timer is over, pop the enemies if enemies there are
			if (timeToNextLevel <= currentTime && enemiesToPop.size() > 0)
			{
				// check if there are some enemies to pop on the map
				List<MapElement>	startPoints = pathFinder.getStartPoints();
				for (int i = 0; i < startPoints.size() && enemiesToPop.size() > 0; i++)
				{
					Enemy enemyToPop = enemiesToPop.remove(0);
					enemyToPop.setX(startPoints.get(i).getX() + 0.5f);
					enemyToPop.setY(startPoints.get(i).getY() + 0.5f);
					enemies.add(enemyToPop);
					System.out.println("New Enemy : ");
				}
				timeToNextLevel = currentTime + POP_ENEMIES_TIME_LAPSE;
			}
			// set good direction and move enemies to reach endpoints
			for (Enemy anEnemy : enemies)
			{
				if (anEnemy.isFlying() == true)
					anEnemy.setDestination(pathFinder.getFlyingNextNode(new MapElement((int)anEnemy.getX(), (int)anEnemy.getY(), MapElement.EMPTY_SQUARE)));
				else
					anEnemy.setDestination(pathFinder.getNextNode(new MapElement((int)anEnemy.getX(), (int)anEnemy.getY(), MapElement.EMPTY_SQUARE)));
				anEnemy.move();
			}
			// build a new Tower if requested
			if (buildingTower != null && mouseMapPosition != null)
			{
				buildNewTower(mouseMapPosition, buildingTower.getType());
				setSelectedTower(mouseMapPosition);
				setMouseMapPosition(null);
			}
			else if (mouseMapPosition != null)
			{
				setSelectedTower(mouseMapPosition);
				setMouseMapPosition(null);
			}
			// try to find appropriate targets for towers and make them fire
			for (Tower aTower : towers)
			{
				// verify that the current tower's target is still in range
				aTower.revalidateTarget();
				// try to find a target if it has not
				aTower.findNewTarget(enemies);
				// fire
				aTower.fire();
			}
			// move projectiles and reach targets
			for (Projectile aProjectile : projectiles)
				aProjectile.move();
			// empty all the toDelete lists
			clearToDeleteLists();
			// re-render everything
			getDisplayable().redraw();
			// wait until the next frame
			executingLoopTime = System.currentTimeMillis() - executingLoopTime;
			if (FRAME_DURATION - executingLoopTime > 0)
			{
				try {
					Thread.sleep(FRAME_DURATION - executingLoopTime);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		// end of the game (loose/win)
		if (lifePoints > 0)
		{
			JOptionPane.showMessageDialog(displayable.frame, "Score: " + scorePoints + "\nYou played so well bro ! Congrat 8D", "SUCKSES !", JOptionPane.PLAIN_MESSAGE);
			System.out.println("WIN (Score: " + scorePoints + "): Player still alive ! GG !");
		}
		else
		{
			JOptionPane.showMessageDialog(displayable.frame, "Score: " + scorePoints + "\nYou must retry and play better...", "BWAHAHA !!!", JOptionPane.PLAIN_MESSAGE);
			System.out.println("LOOSE (score: " + scorePoints + "): Player is dead... Shame on you...");
		}
		displayable.close();
	}

	/**
	 * Fill the enemiesToPop list from an array of PopEnemies
	 * @param enemiesToPop contains all the enemies to pop for this level
	 */
	private static void	fillEnemiesToPop(PopEnemies[] newEnemiesToPop)
	{
		for (PopEnemies specificEnemiesToPop : newEnemiesToPop)
			for (int i = 0; i < specificEnemiesToPop.enemiesNumber; i++)
				enemiesToPop.add(Enemy.getNewEnemy(specificEnemiesToPop.enemiesID));
	}

	/**
	 * remove objects from the objects lists (towers, projectiles, enemies)
	 * and clear the *ToDelete lists (towersToDelete, projectilesToDelete, enemiesToDelete)
	 */
	private static void	clearToDeleteLists()
	{
		// clear enemies
		for (Enemy tmp : enemiesToDelete)
			removeEnemyFromEnemiesList(tmp);
		enemiesToDelete.clear();
		// clear projectiles
		for (Projectile tmp : projectilesToDelete)
			projectiles.remove(tmp);
		projectilesToDelete.clear();
		// clear towers
		for (Tower tmp : towersToDelete)
			removeTowerFromTowersList(tmp);
		towersToDelete.clear();
	}
	
	/**
	 * Check if a given position on the map is buildable or not
	 * @param cell the position to check
	 * @return true if this cell position is buildable, false else
	 */
	public static boolean	isCellBuildable(MapElement cell)
	{
		if (map.getMapElement(cell.getX(), cell.getY()).getType() != MapElement.EMPTY_SQUARE
			|| pathFinder.blockPath(cell)
			|| isCellOccupied(cell))
			return false;
		else
			return true;
	}
	
	/**
	 * Check if there are enemies on the cell or not
	 * @param cell the cell to check
	 * @return true if there is a least 1 enemy on the cell, false else
	 */
	private static boolean isCellOccupied(MapElement cell)
	{
		for (Enemy anEnemy : enemies)
		{
			if ((int)anEnemy.getX() == cell.getX() && (int)anEnemy.getY() == cell.getY())
				return true;
		}
		return false;
	}

	/**
	 * try to build a new tower of the specified type at the specified position
	 * @param cell the position to build the tower
	 * @param towerType the type of the tower to build
	 * @return true if the newTower has been build, false else
	 */
	public static boolean	buildNewTower(MapElement cell, int towerType)
	{
		if (Tower.getTowerTypes().get(towerType).getPrice() <= moneyPoints && isCellBuildable(cell))
		{
			Tower	newTower = Tower.getNewTower(towerType);
			newTower.setX(cell.getX() + 0.5f);
			newTower.setY(cell.getY() + 0.5f);
			addTowerToTowersList(newTower);
			decreaseMoneyPoints(newTower.getPrice());
			return true;
		}
		return false;
	}
	
	/**
	 * try to upgrade a tower (level++) and decrease the moneyPoints
	 * @param aTower the tower to increase its level
	 * @return true if the tower has been successfully increased, false else
	 */
	public static boolean	increaseTower(Tower aTower)
	{
		int	increasePrice = aTower.getIncreaseLevelPrice();
		if (0 <= increasePrice && increasePrice <= moneyPoints && aTower.increaseLevel())
			decreaseMoneyPoints(increasePrice);
		return false;
	}
	
	/**
	 * try to upgrade a tower (level++) and decrease the moneyPoints
	 * @param cell the position of the tower to increase its level
	 * @return true if the tower has been successfully increased, false else
	 */
	public static boolean	increaseTower(MapElement cell)
	{
		Tower aTower = getTower(cell);
		if (aTower != null)
			return increaseTower(aTower);
		return false;
	}

	/**
	 * find and return a Tower using a position
	 * @param cell the position of the Tower that this method will try to return
	 * @return a ref to the found Tower or null if no Tower has been found
	 */
	public static Tower		getTower(MapElement cell)
	{
		for (Tower aTower : towers)
		{
			if ((int)aTower.getX() == cell.getX() && (int)aTower.getY() == cell.getY())
				return aTower;
		}
		return null;
	}
	
	/**
	 * try to remove a tower at the specified position
	 * @param towerToDestroy the Tower to remove
	 * @return true if a Tower has been destroyed, false else
	 */
	public static boolean	destroyTower(Tower towerToDestroy)
	{
		// find the relative tower to the given cell
		// TODO improvement could be done by storing a ref to a Tower in the MapElement class
		if (towerToDestroy != null)
		{
			addTowersToDelete(towerToDestroy);
			// TODO should we refund a part of the money that the player paid for ? (or not)
			return true;
		}
		return false;
	}
	
	/**
	 * try to remove a tower at the specified position
	 * @param cell the position of the Tower to remove
	 * @return true if a Tower has been destroyed, false else
	 */
	public static boolean	destroyTower(MapElement cell)
	{
		return 	destroyTower(getTower(cell));
	}
	
	/**
	 * remove the given Enemy to the enemies list and update Towers' and Projectiles' references
	 * @param anEnemy the Enemy to remove
	 */
	private static void		removeEnemyFromEnemiesList(Enemy anEnemy)
	{
		// take the money
		if (anEnemy.getLife() <= 0)
		{
			increaseMoneyPoints(anEnemy.getGivenMoney());
			increaseScorePoints(anEnemy.getGivenMoney());
			System.out.println("Enemy killed (+" + anEnemy.getGivenMoney() + " gold)");
		}
		else
			System.out.println("Enemy left (" + anEnemy.getDamage() + " damage)");
		// remove enemy from list
		enemies.remove(anEnemy);
		// remove towers' reference to this enemy
		for (Tower aTower : towers)
		{
			if (aTower.getTarget() == anEnemy)
				aTower.setTarget(null);
		}
		// remove projectiles' reference to this enemy
		for (Projectile aProjectile : projectiles)
		{
			if (aProjectile.getTarget() == anEnemy)
				addProjectileToDelete(aProjectile);
		}
	}
	
	/**
	 * add the given Tower to the towers list and update the map's grid content
	 * @param aTower the Tower to add in the list and on the map's grid
	 */
	private static void		addTowerToTowersList(Tower aTower)
	{
		// add tower to list
		towers.add(aTower);
		// update map
		map.setMapElement((int)aTower.getX(), (int)aTower.getY(), MapElement.TOWER);
		// update pathfinder
		pathFinder.modifyGameMap(map);
		//pathFinder.printAllPath();
		// TODO improvement : set a ref to the tower in the relative MapElement
	}
	
	/**
	 * remove the given Tower to the towers list and update the map's grid content
	 * @param aTower the Tower to remove in the list and on the map's grid
	 */
	private static void		removeTowerFromTowersList(Tower aTower)
	{
		// check for dependencies
		if (aTower == buildingTower)
			setBuildingTower(null);
		if (aTower == selectedTower)
			setSelectedTower((Tower)null);
		// add tower to list
		towers.remove(aTower);
		// update map
		map.setMapElement((int)aTower.getX(), (int)aTower.getY(), MapElement.EMPTY_SQUARE);
		// update pathfinder
		pathFinder.modifyGameMap(map);
		//pathFinder.printAllPath();
		// TODO improvement : reset a null ref to the tower in the relative MapElement
	}
}
