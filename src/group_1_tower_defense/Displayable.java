package group_1_tower_defense;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JFrame;
import group_1_tower_defense.displayable_entities.MenuPanel;
import group_1_tower_defense.displayable_entities.Renderer;

/**
 * 
 * @author Frank Lima
 *
 */
public class Displayable
{
	/**
	 * refresh all the render
	 * do not need param, everything is accessible from the Game class via the getters
	 */
	
	//There will be blank space here but please do not adjust the window height to over 600, this will change the calculations of the positions of objects
	//this will be fixed eventually ;)
	public static final int	windowWidth = 1300;
	public static final int	windowHeight = 600;
	
	private GLProfile		profile = GLProfile.get(GLProfile.GL2);
	private GLCapabilities	capabilities = new GLCapabilities(profile);
	private GLCanvas		glcanvas = new GLCanvas(capabilities);
	private Renderer		render = new Renderer(glcanvas);
	public MenuPanel		mp;
	public JFrame			frame;
	
	/**
	 * init the Displayable object
	 */
	public void init()
	{
		// setup OpenGL Version 2

		GLProfile profile = GLProfile.get(GLProfile.GL2);
		GLCapabilities capabilities = new GLCapabilities(profile);


		// The canvas is the widget that's drawn in the JFrame
		GLCanvas glcanvas = new GLCanvas(capabilities);
		Renderer render = new Renderer(glcanvas);

		glcanvas.addGLEventListener(render);
		glcanvas.addMouseListener(render);
		glcanvas.addKeyListener(render);
		glcanvas.addMouseMotionListener(render);
		
		glcanvas.setSize(800, 600);
		
		frame = new JFrame( "Tower Defense" );
		mp = new MenuPanel();
		frame.getContentPane().add(mp.menuRight);
		frame.getContentPane().add(glcanvas);

		// shutdown the program on windows close event
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent ev) {
				System.exit(0);
			}
		});
		
		//frame.setSize( frame.getContentPane().getPreferredSize() );
		frame.setSize(new Dimension(windowWidth, windowHeight));
		frame.setVisible( true );
		
		//Play MP3 here
		new Audio("assets/BG.wav").start();

	}
	
	/**
	 * redraw the glCanvas using the displyableItems from Game
	 */
	public void		redraw()
	{
		//calculate what is needed here	
		render.redraw();
	}
	
	/**
	 * close the Window like clicking the [X] button
	 */
	public void		close()
	{
		WindowEvent wev = new WindowEvent(frame, WindowEvent.WINDOW_CLOSING);
		Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wev);
	}
}
