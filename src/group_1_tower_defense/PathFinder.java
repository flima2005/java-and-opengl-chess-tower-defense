package group_1_tower_defense;

import group_1_tower_defense.game_entities.Map;
import group_1_tower_defense.game_entities.MapElement;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Guillaume Lebedel
 * 
 * The pathFinder class goal is to find the shortest path for any square of the map
 */

public class PathFinder
{
	public static int MaxAdjNodes = 4;
	public static int StartingPoint = 0;
	public static int UnusableNodes = -1;
	private List<MapElement> endPoints = null;
	private List<MapElement> startPoints = null;
	private int[][][] numbers = null;
	private Map gameMap = null;

	/**
	 * One of these methods have to be called in order to initialize the class
	 * 
	 * @param finish is the square of the end of the path or list of squares
	 * @param newMap is the complete map of the game
	 */
	public void init(MapElement finish, MapElement start, Map newMap)
	{
		setGameMap(newMap);
		addEndPoint(finish);
		addStartPoint(start);
		initNumbers();
	}
	public void init(List<MapElement> finish, List<MapElement> start, Map newMap)
	{
		setGameMap(newMap);
		addEndPoint(finish);
		addStartPoint(start);
		initNumbers();
	}	
	public void init(Map newMap)
	{
		setGameMap(newMap);
		findEndPoints(newMap);
		findStartPoints(newMap);
		initNumbers();
	}

	public void findEndPoints(Map newMap)
	{
		if (newMap == null || newMap.getGrid() == null)
			return ;
		MapElement[][] grid = newMap.getGrid();
		int width = newMap.getSizeX();
		int height = newMap.getSizeY();
		for (int i = 0, j; i < height; i++)
			for (j = 0; j < width; j++)
				if (grid[i][j].getType() == MapElement.END_POINT)
					addEndPoint(grid[i][j]);
		System.out.print("Number of end points found : " + endPoints.size() + "\n");
	}

	public void findStartPoints(Map newMap)
	{
		if (newMap == null || newMap.getGrid() == null)
			return ;
		MapElement[][] grid = newMap.getGrid();
		int width = newMap.getSizeX();
		int height = newMap.getSizeY();
		for (int i = 0, j; i < height; i++)
			for (j = 0; j < width; j++)
				if (grid[i][j].getType() == MapElement.START_POINT)
					addStartPoint(grid[i][j]);
		System.out.print("Number of start points found : " + startPoints.size() + "\n");
	}
	public MapElement getNextNode(MapElement ref)
	{
		int [][]rightNumbers = rightNumberMap(ref);
		if (rightNumbers[ref.getY()][ref.getX()] == StartingPoint)
			return ref;
		MapElement[] close = closeNodes(rightNumbers, ref);
		MapElement res = close[0];
		for (int i = 0; close[i] != null; i++)
			if (rightNumbers[close[i].getY()][close[i].getX()] < rightNumbers[res.getY()][res.getX()])
				res = close[i];
		return res;
	}

	public boolean isInPath(MapElement test, List<MapElement> path)
	{
		for (MapElement anElement : path)
			if (test.getX() == anElement.getX() && test.getY() == anElement.getY())
				return true;
		return false;
	}
	/**
	 * This method is to be used when you want to get the whole path (all the square till it reach the endPoints attribute)
	 * @param ref is the square from which you want to get the path
	 * @return the path (a list of MapElement)
	 */
	public List<MapElement> getWholePath(MapElement ref)
	{
		List<MapElement> res = new ArrayList<MapElement>();
		MapElement temp = ref;
		res.add(ref);
		int [][]rightNumbers = rightNumberMap(ref);
		if (temp != null)
			while ((temp = getNextNode(temp)) != null &&  rightNumbers[temp.getY()][temp.getX()] != StartingPoint)
				if (!isInPath(temp, res))
				res.add(temp);
				else
					return res;
		if (temp != null  && rightNumbers[temp.getY()][temp.getX()] == StartingPoint)
			res.add(temp);
		return res;
	}

	public MapElement getFlyingNextNode(MapElement ref)
	{
		MapElement closestEnd = getClosestFlyingEnd(ref);
		int distX = Math.abs(closestEnd.getX() - ref.getX());
		int distY = Math.abs(closestEnd.getY() - ref.getY());
		if (distX == 0 && distY == 0)
			return ref;
		else if (distX >= distY)
		{
			if (ref.getX() > closestEnd.getX())
				return gameMap.getGrid()[ref.getY()][ref.getX() - 1];
			else
				return gameMap.getGrid()[ref.getY()][ref.getX() + 1];
		}
		else
		{
			if (ref.getY() > closestEnd.getY())
				return gameMap.getGrid()[ref.getY() - 1][ref.getX()];
			else
				return gameMap.getGrid()[ref.getY() + 1][ref.getX()];
		}
	}

	public List<MapElement> getFlyingWholePath(MapElement ref)
	{
		List<MapElement> res = new ArrayList<MapElement>();
		MapElement temp = ref;
		MapElement save = null;
		res.add(ref);
		while ((save = getFlyingNextNode(temp)) != null && save != temp)
		{
			res.add(save);
			temp = save;
		}
		return res;
	}

	/**
	 * @param StartPoint is the element of the map for which we want to know if there will still be a valid path
	 * @param ref the element that is going to become a tower
	 * @return true if there is no valid path anymore and false if there still is one.
	 */
	public boolean blockPath(MapElement StartPoint, MapElement ref)
	{
		MapElement[][] srcMap = gameMap.getGrid();
		MapElement[][] tempGrid = new MapElement[gameMap.getSizeY()][gameMap.getSizeX()];
		for (int i =0, j; i < gameMap.getSizeY(); i++)
			for (j = 0; j < gameMap.getSizeX(); j++)
				tempGrid[i][j] = new MapElement(srcMap[i][j]);
		tempGrid[ref.getY()][ref.getX()].setType(MapElement.TOWER);
		PathFinder test = new PathFinder();
		Map tempMap = new Map(gameMap.getSizeX(), gameMap.getSizeY(), tempGrid);
		test.init(endPoints, startPoints, tempMap);

		if (test.getNextNode(StartPoint) == null)
			return true;
		return false;
	}

	public boolean blockPath(List<MapElement> StartPoints, MapElement ref)
	{
		MapElement[][] srcMap = gameMap.getGrid();
		MapElement[][] tempGrid = new MapElement[gameMap.getSizeY()][gameMap.getSizeX()];
		for (int i =0, j; i < gameMap.getSizeY(); i++)
			for (j = 0; j < gameMap.getSizeX(); j++)
				tempGrid[i][j] = new MapElement(srcMap[i][j]);
		tempGrid[ref.getY()][ref.getX()].setType(MapElement.TOWER);
		PathFinder test = new PathFinder();
		Map tempMap = new Map(gameMap.getSizeX(), gameMap.getSizeY(), tempGrid);
		test.init(endPoints, startPoints, tempMap);
		Iterator<MapElement> it = StartPoints.iterator();
		while(it.hasNext())
		{
			if (test.getNextNode(it.next()) == null)
				return true;
		}
		return false;
	}

	public boolean blockPath(MapElement ref)
	{
		return blockPath(startPoints, ref);
	}

	/**
	 * 
	 * @param ref
	 * @return the array of numbers corresponding to the right end square which allows to find the shortest path
	 */
	private int[][] rightNumberMap(MapElement ref)
	{
		int res = 0;
		int	j = endPoints.size();	
		int i;
		int temp = -1;
		for (i = 0; i < j; i++)
			if (temp == -1 || numbers[i][ref.getY()][ref.getX()] < temp)
				temp = numbers[(res = i)][ref.getY()][ref.getX()];
		return numbers[res];
	}

	public void printNumberMap(int[][] numberMap)
	{
		if (numberMap == null)
		{
			for (int i = 0; i < endPoints.size(); i++, System.out.print("\n\n"))
				for (int j = 0; j < numbers[i].length; j++, System.out.print("\n"))
					for (int k = 0; k < numbers[i][j].length; k++)
						System.out.print(numbers[i][j][k] + "\t");
		}
		else
			for (int j = 0; j < numberMap.length; j++, System.out.print("\n"))
				for (int k = 0; k < numberMap[j].length; k++)
					System.out.print(numberMap[j][k] + "\t");
	}
	private boolean initNumbers()
	{
		if (endPoints == null || endPoints.size() == 0)
			return false;
		MapElement end;
		numbers = new int [endPoints.size()][gameMap.getSizeY()][gameMap.getSizeX()];
		for (int k = 0, l = endPoints.size(), i, j; k < l ; k++)
		{
			end = endPoints.get(k);
			for (i = 0; i < gameMap.getSizeY(); i++)
				for (j = 0; j < gameMap.getSizeX(); j++)
					numbers[k][i][j] = UnusableNodes;
			numbers[k][end.getY()][end.getX()] = StartingPoint;
			calcNumbers(numbers[k], end, StartingPoint);
		}
		return true;
	}
	/**
	 * This Private method calculate a number for each of the square of the grid. 
	 * Basically the further you are from the goal, the higher this number will be.
	 * @rightNumbers is the right array of numbers (so the right ending point) that is to be used to find the shortest pat.
	 * @param ref is the square of the grid from which we calculate the closest other
	 * @param parent is the value that its parent square had
	 */
	private void calcNumbers(int[][] rightNumbers, MapElement ref, int parent)
	{
		if (ref == null)
			return;
		MapElement[] close = closeNodes(rightNumbers, ref);
		for (int i = 0; close[i] != null; i++)
		{
			if (rightNumbers[close[i].getY()][close[i].getX()] == UnusableNodes || 
					parent + 1 < rightNumbers[close[i].getY()][close[i].getX()])
			{
				rightNumbers[close[i].getY()][close[i].getX()] = parent + 1;
				calcNumbers(rightNumbers, close[i], parent + 1);
			}
		}
	}

	public List<MapElement> getEnd()
	{
		return endPoints;
	}

	public void setEnd(List<MapElement> endPoints)
	{
		this.endPoints = new ArrayList<MapElement>(endPoints);
		initNumbers();
	}

	public void setEnd(MapElement endPoints)
	{
		this.endPoints = new ArrayList<MapElement>();
		this.endPoints.add(endPoints);
		initNumbers();
	}

	public void addEndPoint(List<MapElement> endToAdd)
	{
		if (endPoints == null)
			this.endPoints = new ArrayList<MapElement>();
		endPoints.addAll(endToAdd);
		initNumbers();
	}	

	public void addEndPoint(MapElement endToAdd)
	{
		if (endPoints == null)
			this.endPoints = new ArrayList<MapElement>();
		endPoints.add(endToAdd);
		initNumbers();
	}

	public void addStartPoint(List<MapElement> startToAdd)
	{
		if (startPoints == null)
			this.startPoints = new ArrayList<MapElement>();
		startPoints.addAll(startToAdd);
	}	

	public void addStartPoint(MapElement startToAdd)
	{
		if (startPoints == null)
			this.startPoints = new ArrayList<MapElement>();
		startPoints.add(startToAdd);
	}

	public void deleteEndPoint(MapElement endToDelete)
	{
		endPoints.remove(endToDelete);
	}	

	public void deleteStartPoint(MapElement startToDelete)
	{
		startPoints.remove(startToDelete);
	}

	public Map getGameMap()
	{
		return gameMap;
	}

	public void setGameMap(Map gameMap)
	{
		this.gameMap = gameMap;
	}

	public void modifyGameMap(Map gameMap)
	{
		setGameMap(gameMap);
		initNumbers();
	}


	public PathFinder()
	{
		this.endPoints = new ArrayList<MapElement>();
	}

	/** calculate and print the path for all empty squares of the map **/

	public void printAllPath()
	{
		if (gameMap != null && gameMap.getGrid() != null)
			for (int i = 0,j; i < gameMap.getSizeY(); i++)
				for (j = 0; j < gameMap.getSizeX(); j++)
					if (gameMap.getGrid()[i][j].getType() <= MapElement.CONSTRUCTABLE_LIMIT)
						printPath(getWholePath(gameMap.getGrid()[i][j]));
	}

	/**
	 * Print in an ascii manner a path.
	 * (empty square is a " " and full square is a ".", starting point is an "O", end point a "X" and path element a "=")
	 * Also displays the length of the path 
	 * @param path is the path you want to print
	 */
	public void printPath(List<MapElement> path)
	{
		if (path == null)
			return;
		if (path.size() == 1)
		{
			System.out.print("NO PATH!!!\n");
			return ;
		}
		Iterator<MapElement> test2 = path.iterator();
		MapElement temp = null;
		String[][] tempgrid = new String[gameMap.getSizeY()][gameMap.getSizeX()];
		for (int i =0; i < gameMap.getSizeY(); i++)
			for (int j =0; j < gameMap.getSizeX(); j++)
				if (gameMap.getGrid()[i][j].getType() > MapElement.CONSTRUCTABLE_LIMIT)
					tempgrid[i][j] = ".";
				else if (gameMap.getGrid()[i][j].getType() <= MapElement.CONSTRUCTABLE_LIMIT)
					tempgrid[i][j] = " ";
		while (test2.hasNext())
		{
			temp = test2.next();
			tempgrid[temp.getY()][temp.getX()] = "=";
		}
		tempgrid[temp.getY()][temp.getX()] = "X";
		tempgrid[path.get(0).getY()][path.get(0).getX()] = "O";
		System.out.print("\n");
		for (int i =0; i < gameMap.getSizeY(); i++, System.out.print("\n"))
			for (int j = 0; j < gameMap.getSizeX(); j++)
				System.out.print(tempgrid[i][j] + " ");
		System.out.print("Longueur du path : " + path.size() + "\n");
	}
	public List<MapElement> getStartPoints()
	{
		return startPoints;
	}
	public void setStartPoints(List<MapElement> startPoints)
	{
		this.startPoints = startPoints;
	}
	private MapElement getClosestFlyingEnd(MapElement ref)
	{
		MapElement res = null;
		Iterator<MapElement> it = endPoints.iterator();
		MapElement temp;
		int dist;
		int shortestDist = -1;
		while (it.hasNext())
		{
			temp = it.next();
			dist = Math.abs((temp.getX() - ref.getX()) + (temp.getY() - ref.getY()));
			if (shortestDist == -1 || dist < shortestDist)
			{
				shortestDist = dist;
				res = temp;
			}
		}
		return res;
	}
	/**
	 * Private also
	 * @param ref is the square of the grid from which we want to get the other close squares (the one touching it - left - bottom - right - top)
	 * @return an array (of MapElement) of the close square found and matching the if (no Full square and inside of the grid)
	 */

	private MapElement[] closeNodes(int[][] rightNumbers, MapElement ref)
	{
		if (ref == null)
			return null;
		MapElement[] close = new MapElement[MaxAdjNodes + 1];
		MapElement[][] map = gameMap.getGrid();
		int i = 0;
		if (ref.getX() + 1 < gameMap.getSizeX() && map[ref.getY()][ref.getX() + 1].getType() <= MapElement.CONSTRUCTABLE_LIMIT &&
				rightNumbers[ref.getY()][ref.getX()] != UnusableNodes)
			close[i++] = map[ref.getY()][ref.getX() + 1];
		if (ref.getX() - 1 >= 0  && map[ref.getY()][ref.getX() - 1].getType() <= MapElement.CONSTRUCTABLE_LIMIT &&
				rightNumbers[ref.getY()][ref.getX()] != UnusableNodes)
			close[i++] = map[ref.getY()][ref.getX() - 1];
		if (ref.getY() + 1 < gameMap.getSizeY() && map[ref.getY() + 1][ref.getX()].getType() <= MapElement.CONSTRUCTABLE_LIMIT &&
				rightNumbers[ref.getY()][ref.getX()] != UnusableNodes)
			close[i++] = map[ref.getY() + 1][ref.getX()];
		if (ref.getY() - 1 >= 0  && map[ref.getY() - 1][ref.getX()].getType() <= MapElement.CONSTRUCTABLE_LIMIT &&
				rightNumbers[ref.getY()][ref.getX()] != UnusableNodes)
			close[i++] = map[ref.getY() - 1][ref.getX()];
		close[i] = null;
		return close;
	}
}
